﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NativeInteropEx;
using buffer = NativeInteropEx.Buffer;
using System.Runtime.CompilerServices;

namespace STLReadWrite
{
    static class Methods
    {
        public static void BinaryReader(byte[] triBytes, STLTriangle[] tris) {
            using (var br = new BinaryReader(new MemoryStream(triBytes))) {
                for (int i = 0; i < tris.Length; ++i) {
                    var normX = br.ReadSingle();
                    var normY = br.ReadSingle();
                    var normZ = br.ReadSingle();
                    var aX = br.ReadSingle();
                    var aY = br.ReadSingle();
                    var aZ = br.ReadSingle();
                    var bX = br.ReadSingle();
                    var bY = br.ReadSingle();
                    var bZ = br.ReadSingle();
                    var cX = br.ReadSingle();
                    var cY = br.ReadSingle();
                    var cZ = br.ReadSingle();
                    var abc = br.ReadUInt16();
                    tris[i] = new STLTriangle(
                        new STLVector(normX, normY, normZ),
                        new STLVector(aX, aY, aZ),
                        new STLVector(bX, bY, bZ),
                        new STLVector(cX, cY, cZ),
                        abc);
                }
            }
        }

        // reading all the data at once and then extracting individual fields using BitConverter
        public static void BitConverter(byte[] triBytes, STLTriangle[] tris) {
            for (int i = 0; i < tris.Length; ++i) {
                var offset = i * STLTriangle.Size;
                var normX = System.BitConverter.ToSingle(triBytes, offset);
                var normY = System.BitConverter.ToSingle(triBytes, offset + 4);
                var normZ = System.BitConverter.ToSingle(triBytes, offset + 8);
                var aX = System.BitConverter.ToSingle(triBytes, offset + 12);
                var aY = System.BitConverter.ToSingle(triBytes, offset + 16);
                var aZ = System.BitConverter.ToSingle(triBytes, offset + 20);
                var bX = System.BitConverter.ToSingle(triBytes, offset + 24);
                var bY = System.BitConverter.ToSingle(triBytes, offset + 28);
                var bZ = System.BitConverter.ToSingle(triBytes, offset + 32);
                var cX = System.BitConverter.ToSingle(triBytes, offset + 36);
                var cY = System.BitConverter.ToSingle(triBytes, offset + 40);
                var cZ = System.BitConverter.ToSingle(triBytes, offset + 44);
                var abc = System.BitConverter.ToUInt16(triBytes, offset + 48);
                tris[i] = new STLTriangle(
                    new STLVector(normX, normY, normZ),
                    new STLVector(aX, aY, aZ),
                    new STLVector(bX, bY, bZ),
                    new STLVector(cX, cY, cZ),
                    abc);
            }
        }

        public static void BlockCopy(byte[] triBytes, STLTriangle[] tris) {
            var coords = new float[12];
            for (int i = 0; i < tris.Length; ++i) {
                var offset = i * STLTriangle.Size;
                System.Buffer.BlockCopy(triBytes, offset, coords, 0, 48);
                var abc = System.BitConverter.ToUInt16(triBytes, offset + 48);
                tris[i] = new STLTriangle(
                    new STLVector(coords[0], coords[1], coords[2]),
                    new STLVector(coords[3], coords[4], coords[5]),
                    new STLVector(coords[6], coords[7], coords[8]),
                    new STLVector(coords[9], coords[10], coords[11]),
                    abc);
            }
        }

        [DllImport("msvcrt.dll", EntryPoint = "memcpy", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        static extern IntPtr memcpy(IntPtr dest, IntPtr src, UIntPtr count);

        public static unsafe void Memcpy(byte[] triBytes, STLTriangle[] tris) {
            fixed (byte* src = triBytes)
            fixed (STLTriangle* dst = tris)
            {
                memcpy(new IntPtr(dst), new IntPtr(src), new UIntPtr((uint)triBytes.Length));
            }
        }

        // reading all the data at once and then extracting individual fields using BitConverter
        public static unsafe void MemcpyMultiThreaded(byte[] triBytes, STLTriangle[] tris) {
            var threadcount = Environment.ProcessorCount;
            var chunksize = triBytes.Length / threadcount;
            var remainder = triBytes.Length % threadcount;

            fixed (byte* pBytes = &triBytes[0])
            fixed (STLTriangle* pTris = &tris[0])
            {
                var tasks = new Action[threadcount];
                for (var i = 0; i < threadcount - 1; ++i) {
                    var offset = i * chunksize;
                    var newSrc = new IntPtr(pBytes + offset);
                    var newDst = new IntPtr((byte*)pTris + offset);
                    tasks[i] = () => memcpy(newSrc, newDst, new UIntPtr((uint)chunksize));
                }

                var finalOffset = (threadcount - 1) * chunksize;
                var finalSrc = new IntPtr(pBytes + finalOffset);
                var finalDst = new IntPtr((byte*)pTris + finalOffset);
                tasks[threadcount - 1] = () => memcpy(finalSrc, finalDst, new UIntPtr((uint)(chunksize + remainder)));

                Parallel.Invoke(tasks);
            }
        }

        public static unsafe void BufferMemoryCopy(byte[] triBytes, STLTriangle[] tris) {
            fixed (byte* src = triBytes)
            fixed (STLTriangle* dst = tris)
            {
                System.Buffer.MemoryCopy(src, dst, triBytes.LongLength, triBytes.LongLength);
            }
        }

        public static unsafe void BufferMemoryCopyMultiThreaded(byte[] triBytes, STLTriangle[] tris) {
            var threadcount = Environment.ProcessorCount;
            var chunksize = triBytes.Length / threadcount;
            var remainder = triBytes.Length % threadcount;

            fixed (byte* pBytes = &triBytes[0])
            fixed (STLTriangle* pTris = &tris[0])
            {
                var tasks = new Action[threadcount];
                for (var i = 0; i < threadcount - 1; ++i) {
                    var offset = i * chunksize;
                    var newSrc = pBytes + offset;
                    var newDst = (byte*)pTris + offset;
                    tasks[i] = () => System.Buffer.MemoryCopy(newSrc, newDst, chunksize, chunksize);
                }

                var finalOffset = (threadcount - 1) * chunksize;
                var finalSrc = pBytes + finalOffset;
                var finalDst = (byte*)pTris + finalOffset;
                tasks[threadcount - 1] = () => System.Buffer.MemoryCopy(finalSrc, finalDst, chunksize + remainder, chunksize + remainder);

                Parallel.Invoke(tasks);
            }
        }

        public static void BufferConvert(byte[] triBytes, STLTriangle[] tris) {
            buffer.Copy(triBytes, tris);            
        }

        public static unsafe void BufferConvertFixed(byte[] triBytes, STLTriangle[] tris) {
            fixed (byte* pBytes = &triBytes[0])
            fixed (STLTriangle* pTris = &tris[0])
            {
                buffer.Copy(new IntPtr(pBytes), new IntPtr(pTris), new IntPtr(triBytes.Length));
            }
        }

        public static unsafe void IntPtrGet(byte[] triBytes, STLTriangle[] tris) {
            fixed (byte* pBytes = &triBytes[0])
            {
                var ptr = new IntPtr(pBytes);
                for (var i = 0; i < tris.Length; ++i) {
                    tris[i] = ptr.Get<STLTriangle>(i);
                }
            }
        }

        public static void ReinterpretCast(byte[] triBytes, STLTriangle[] tris) {
            using (var rc = new ReinterpretCast<byte[], STLTriangle>(triBytes)) {
                for (var i = 0; i < tris.Length; ++i) {
                    tris[i] = rc[i];
                }
            }
        }

        public static unsafe void BufferCopyMultiThreaded(byte[] triBytes, STLTriangle[] tris) {
            var threadcount = Environment.ProcessorCount;
            var chunksize = triBytes.Length / threadcount;
            var remainder = triBytes.Length % threadcount;

            fixed (byte* pBytes = &triBytes[0])
            fixed (STLTriangle* pTris = &tris[0])
            {
                var tasks = new Action[threadcount];
                for (var i = 0; i < threadcount - 1; ++i) {
                    var offset = i * chunksize;
                    var newSrc = new IntPtr(pBytes + offset);
                    var newDst = new IntPtr((byte*)pTris + offset);
                    tasks[i] = () => buffer.Copy(newSrc, newDst, new IntPtr(chunksize));
                }

                var finalOffset = (threadcount - 1) * chunksize;
                var finalSrc = new IntPtr(pBytes + finalOffset);
                var finalDst = new IntPtr((byte*)pTris + finalOffset);
                tasks[threadcount - 1] = () => buffer.Copy(finalSrc, finalDst, new IntPtr(chunksize + remainder));

                Parallel.Invoke(tasks);
            }
        }

        // reading structs using Marshal.Copy
        public static unsafe void MarshalCopy(byte[] triBytes, STLTriangle[] tris) {
            fixed (STLTriangle* pTris = &tris[0])
            {
                Marshal.Copy(triBytes, 0, new IntPtr(pTris), triBytes.Length);
            }
        }

        public static unsafe void MarshalCopyMultiThreaded(byte[] triBytes, STLTriangle[] tris) {
            var threadcount = Environment.ProcessorCount;
            var chunksize = triBytes.Length / threadcount;
            var remainder = triBytes.Length % threadcount;

            fixed (STLTriangle* pTris = &tris[0])
            {
                var tasks = new Action[threadcount];
                for (var i = 0; i < threadcount - 1; ++i) {
                    var offset = i * chunksize;
                    var newDst = new IntPtr((byte*)pTris + offset);
                    tasks[i] = () => Marshal.Copy(triBytes, offset, newDst, chunksize);
                }

                var finalOffset = (threadcount - 1) * chunksize;
                var finalDst = new IntPtr((byte*)pTris + finalOffset);
                tasks[threadcount - 1] = () => Marshal.Copy(triBytes, finalOffset, finalDst, chunksize + remainder);

                Parallel.Invoke(tasks);
            }
        }

        // reading structs using Marshal.PtrToStructure (reading stream all at once)
        public static unsafe void MarshalPtrToStructure(byte[] triBytes, STLTriangle[] tris) {
            var triType = typeof(STLTriangle);
            fixed (byte* pBytes = &triBytes[0])
            {
                for (int i = 0; i < tris.Length; ++i) {
                    var offset = i * STLTriangle.Size;
                    tris[i] = (STLTriangle)Marshal.PtrToStructure(new IntPtr(pBytes + offset), triType);
                }
            }
        }

        // reading structs using Marshal.PtrToStructure (reading stream all at once)
        public static unsafe void MarshalPtrToStructureGeneric(byte[] triBytes, STLTriangle[] tris) {
            var triType = typeof(STLTriangle);
            fixed (byte* pBytes = &triBytes[0])
            {
                for (int i = 0; i < tris.Length; ++i) {
                    var offset = i * STLTriangle.Size;
                    tris[i] = Marshal.PtrToStructure<STLTriangle>(new IntPtr(pBytes + offset));
                }
            }
        }

        public static unsafe void UnsafePointerArith(byte[] triBytes, STLTriangle[] tris) {
            fixed (byte* pbuffer = triBytes)
            fixed (STLTriangle* ptris = tris)
            {
                var pSrc = (STLTriangle*)pbuffer;
                var pDst = ptris;
                for (int i = 0; i < tris.Length; ++i) {
                    *pDst++ = *pSrc++;
                }
            }
        }

        // parameters: src, dst, length (bytes)
        static Action<IntPtr, IntPtr, uint> CpBlkIL = GenerateCpBlk();

        static Action<IntPtr, IntPtr, uint> GenerateCpBlk() {
            var emitter = Sigil.Emit<Action<IntPtr, IntPtr, uint>>.NewDynamicMethod("CopyBlockIL");
            // emit IL
            emitter.LoadArgument(1); // dest
            emitter.LoadArgument(0); // src
            emitter.LoadArgument(2); // len
            emitter.CopyBlock();
            emitter.Return();
            // compile to delegate
            return emitter.CreateDelegate();
        }

        public static unsafe void CpBlk(byte[] triBytes, STLTriangle[] tris) {
            fixed (byte* src = triBytes)
            fixed (STLTriangle* dst = tris)
            {
                CpBlkIL(new IntPtr(src), new IntPtr(dst), (uint)triBytes.Length);
            }
        }

        public static void CpBlkGCAlloc(byte[] triBytes, STLTriangle[] tris) {
            var srcHandle = GCHandle.Alloc(triBytes, GCHandleType.Pinned);
            var dstHandle = GCHandle.Alloc(tris, GCHandleType.Pinned);

            CpBlkIL(srcHandle.AddrOfPinnedObject(), dstHandle.AddrOfPinnedObject(), (uint)triBytes.Length);

            srcHandle.Free();
            dstHandle.Free();
        }

        public static unsafe void CpBlkMT(byte[] triBytes, STLTriangle[] tris) {
            var threadcount = Environment.ProcessorCount;
            var chunksize = triBytes.Length / threadcount;
            var remainder = triBytes.Length % threadcount;

            fixed (byte* pBytes = &triBytes[0])
            fixed (STLTriangle* pTris = &tris[0])
            {
                var tasks = new Action[threadcount];
                for (var i = 0; i < threadcount - 1; ++i) {
                    var offset = i * chunksize;
                    var newSrc = new IntPtr(pBytes + offset);
                    var newDst = new IntPtr((byte*)pTris + offset);
                    tasks[i] = () => CpBlkIL(newSrc, newDst, (uint)chunksize);
                }

                var finalOffset = (threadcount - 1) * chunksize;
                var finalSrc = new IntPtr(pBytes + finalOffset);
                var finalDst = new IntPtr((byte*)pTris + finalOffset);
                tasks[threadcount - 1] = () => CpBlkIL(finalSrc, finalDst, (uint)(chunksize + remainder));

                Parallel.Invoke(tasks);
            }
        }

        [StructLayout(LayoutKind.Explicit)]
        struct ByteTriangleUnion
        {
            [FieldOffset(0)]
            public byte[] bytes;

            [FieldOffset(0)]
            public STLTriangle[] triangles;
        }

        public static void UnionStruct(byte[] triBytes, STLTriangle[] tris) {
            var union = new ByteTriangleUnion { bytes = triBytes };
            for (var i = 0; i < union.triangles.Length / 50; ++i) {
                tris[i] = union.triangles[i];
            }
        }

        public static unsafe void CompilerServicesUnsafeCopyBlock(byte[] triBytes, STLTriangle[] tris)
        {
            fixed (byte* src = triBytes)
            fixed (STLTriangle* dst = tris) {
                Unsafe.CopyBlock(dst, src, (uint)triBytes.Length);
            }
        }

        public static unsafe void CompilerServicesUnsafeCopy(byte[] triBytes, STLTriangle[] tris)
        {
            fixed (byte* src = triBytes)
            fixed (STLTriangle* dst = tris) {
                for (var i = 0; i < tris.Length; ++i) {
                    Unsafe.Copy(ref tris[i], (STLTriangle*)src + i);
                }
            }
        }
    }
}
