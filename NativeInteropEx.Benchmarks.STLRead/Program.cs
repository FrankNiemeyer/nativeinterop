﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NativeInteropEx;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Threading;
using static STLReadWrite.Methods;

namespace STLReadWrite
{   
    class Program
    {
        //static string filename = "mymodel.stl";
        //static string filename = "Zeichnung1.stl";
        static string filename = "Huefte_Femur.stl";

        const int bytesPerTriangle = 50;
        const int runs = 10000;
        static readonly TimeSpan timeOut = TimeSpan.FromSeconds(5.0);
        const int maxExp = 21;

        static void Main(string[] args) {
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            Console.WriteLine("High res: {0}", Stopwatch.IsHighResolution);
            var resolution = 1E9 / (double)Stopwatch.Frequency;
            Console.WriteLine("Resolution: {0} nanoseconds", resolution);
            // = 300 ns
            
            // true STL data
            /*
            var header = "";
            byte[] triBytes;            
            using (var br = new BinaryReader(File.OpenRead(filename), Encoding.ASCII)) {
                header = Encoding.ASCII.GetString(br.ReadBytes(80));
                var triCount = br.ReadUInt32();
                triBytes = br.ReadBytes((int)triCount * STLTriangle.Size);
            }*/

            var triCounts = Enumerable
                .Range(0, maxExp)
                .Select(i => (int)Math.Pow(2.0, i))
                .Skip(6)
                .ToArray();
            
            var benchmarks = new (string name, Action<byte[], STLTriangle[]> action)[] {
                //("*dst++ = *src++", UnsafePointerArith),
                //("BinaryReader", BinaryReader),
                //("BitConverter", BitConverter),
                //("Buffer.BlockCopy", BlockCopy),
                ("Buffer.Copy[T,U] fixed", BufferConvertFixed),
                //("Buffer.Copy[T,U]", BufferConvert),
                //("Buffer.Copy<T,U> MT", BufferCopyMultiThreaded),
                ("cpblk", CpBlk),
                //("cpblk GCHandle", CpBlkGCAlloc),
                //("cpblk MT", CpBlkMT),
                //("Marshal.Copy", MarshalCopy),
                //("Marshal.Copy MT", MarshalCopyMultiThreaded),
                //("Marshal.PtrToStructure", MarshalPtrToStructure),
                //("Marshal.PtrToStructure<T>", MarshalPtrToStructureGeneric),
                //("memcpy", Memcpy),
                //("memcpy MT", MemcpyMultiThreaded),
                //("IntPtr.Get<T>", IntPtrGet),
                //("ReinterpretCast", ReinterpretCast),
                //("UnionStruct", UnionStruct),
                //("MemoryCopy", BufferMemoryCopy),
                //("MemoryCopy MT", BufferMemoryCopyMultiThreaded),
                ("CompilerServices.Unsafe.CopyBlock", CompilerServicesUnsafeCopyBlock),
                ("CompilerServices.Unsafe.Copy", CompilerServicesUnsafeCopy)
            };

            foreach (var benchmark in benchmarks) {                
                Console.WriteLine(string.Format("-- {0} ", benchmark.name).PadRight(79, '-'));
                foreach (var triangleCount in triCounts) {
                    // generate fake data                    
                    var triBytes = new byte[bytesPerTriangle * triangleCount];
                    // result
                    var triangles = new STLTriangle[triangleCount];
                    RunStatistics.Benchmark(triangleCount + " tris", runs, timeOut, () => benchmark.action(triBytes, triangles));
                }
            }
        }
    }
}