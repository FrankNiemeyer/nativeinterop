﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STLReadWrite
{
    class RunStatistics
    {
        public readonly int Runs;
        public readonly double Total;
        public readonly double Average;
        public readonly double Min;
        public readonly double Max;
        public readonly double Median;
        public readonly double PercentileAverage;
        public IEnumerable<double> Timings;

        public RunStatistics(int runs, double total, double average, double min, double max, double median, double percentileAverage, IEnumerable<double> timings) {
            this.Runs = runs;
            this.Total = total;
            this.Average = average;
            this.Min = min;
            this.Max = max;
            this.Median = median;
            this.PercentileAverage = percentileAverage;
            this.Timings = timings;
        }

        public static RunStatistics RunNTimes(int runs, TimeSpan timeOut, Action action, double percentile = 0.10) {
            //var warmUpTimeOut = TimeSpan.FromMilliseconds(500.0);

            var timings = new List<double>();
            var swTimeOut = Stopwatch.StartNew();
            var swPerRun = new Stopwatch();
            var swTotal = new Stopwatch();

            //// warm up
            //for (var i = 0; i < 1000; ++i) {
            //    GC.Collect();
            //    GC.WaitForPendingFinalizers();
            //    swTotal.Start();
            //    swPerRun.Restart();
            //    action();
            //    swPerRun.Stop();
            //    swTotal.Stop();
            //    timings.Add(swPerRun.Elapsed.TotalMilliseconds);
            //    if (swTimeOut.Elapsed > timeOut)
            //        break;
            //}

            //while (timings.Count > 0)
            //    timings.RemoveAt(timings.Count - 1);
            swPerRun.Reset();
            swTotal.Reset();
            swTimeOut.Restart();
            for (var i = 0; i < runs; ++i) {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                swTotal.Start();
                swPerRun.Restart();
                action();
                swPerRun.Stop();
                swTotal.Stop();
                timings.Add(swPerRun.Elapsed.TotalMilliseconds);
                if (swTimeOut.Elapsed > timeOut)
                    break;
            }
            swTimeOut.Stop();

            timings.Sort();
            var percIndex = (int)(percentile * timings.Count);
            var belowPerc = new double[percIndex];
            timings.CopyTo(0, belowPerc, 0, belowPerc.Length);
            
            return new RunStatistics(
                timings.Count,
                swTotal.Elapsed.TotalMilliseconds,// timings.Sum(),
                timings.Min(),
                timings.Max(),
                swTotal.Elapsed.TotalMilliseconds / timings.Count,// timings.Average(),
                timings[timings.Count / 2],
                belowPerc.Average(),
                timings);
        }

        public static void Benchmark(string descr, int runs, TimeSpan timeOut, Action action) {
            var rs = RunNTimes(runs, timeOut, action);
            var s = string.Format("{0}: ran {1,6}, avg: {2,10:0.00000}, 10th perc.avg.: {3,10:0.00000} ", descr, rs.Runs, rs.Average, rs.PercentileAverage);
            Console.WriteLine(s.PadLeft(79, ' '));
            //File.WriteAllLines(descr + ".txt", rs.Timings.Select(t => t.ToString()));
        }
    }
}

