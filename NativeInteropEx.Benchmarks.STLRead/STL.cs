﻿using NativeInteropEx;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace STLReadWrite
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct STLVector
    {
        public readonly float X;
        public readonly float Y;
        public readonly float Z;

        public STLVector(float _x, float _y, float _z) {
            X = _x;
            Y = _y;
            Z = _z;
        }

        public override string ToString() {
            return string.Format("[{0}, {1}, {2}]", X, Y, Z);
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct STLTriangle
    {
        public const int Size = 50;

        // 4 * 3 * 4 byte + 2 byte = 50 byte
        public readonly STLVector Normal;
        public readonly STLVector A;
        public readonly STLVector B;
        public readonly STLVector C;
        public readonly ushort AttributeByteCount;

        public STLTriangle(STLVector normalVec, STLVector vertex1, STLVector vertex2, STLVector vertex3, ushort attr = 0) {
            Normal = normalVec;
            A = vertex1;
            B = vertex2;
            C = vertex3;
            AttributeByteCount = attr;
        }

        public override string ToString() {
            return string.Format("Normal: {0}\nVertex A: {1}\nVertex B: {2}\nVertex C: {3}\nAttributeByteCount: {4}", Normal, A, B, C, AttributeByteCount);
        }
    }

    class STLModel
    {
        public string Description { get; private set; }
        public IEnumerable<STLTriangle> Faces { get; private set; }

        public STLModel(IEnumerable<STLTriangle> faces, string modelDesc = "", bool copyFaces = true) {
            if (copyFaces) {
                Faces = new List<STLTriangle>(faces);
            }
            else {
                Faces = faces;
            }
            Description = modelDesc;
        }

        public void SaveAsBinaryFile(string path) {
            using (var bw = new BinaryWriter(File.OpenWrite(path), Encoding.ASCII)) {
                var header = new byte[80];
                var charCount = Description.Length < header.Length ? Description.Length : header.Length;
                Encoding.ASCII.GetBytes(Description, 0, charCount, header, 0);
                bw.Write(header);
                bw.Write(Faces.Count());
                bw.BaseStream.WriteUnmanagedStructRange(Faces.ToArray());
            }
        }
    }
}
