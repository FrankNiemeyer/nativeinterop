﻿open NativeInteropEx

open System
open System.Diagnostics

type N = float32

let reps = 10000000
let veclen = 2048
let scale = 1.0f / (float32 veclen)
let flops = 5L * int64 veclen * int64 reps

let inline square x = x * x
let inline sqacc acc x = acc + x * x

let timeit pre f =
    printf "Running case: %A" pre
    let sw = Stopwatch.StartNew ()
    let res = f ()
    sw.Stop ()
    let perf = float (flops / 1000L) / sw.Elapsed.TotalMilliseconds
    let sum = res |> NativeArray.SIMD.sum
    printfn " --> Perf: %g MFLOP/s; res = %g" perf sum

let manualPrealloc (xs: NativeArray<N>) (ys: NativeArray<N>) (zs: NativeArray<N>) (dp: NativeArray<N>) =    
    for j = 1 to reps do
        let mutable dpv = dp |> VectorIndexer
        let vxs = xs |> VectorIndexer
        let vys = ys |> VectorIndexer
        let vzs = zs |> VectorIndexer
        let mutable i = 0L
        while i < xs.Length - NativeArray.SIMD.stride<N> do
            dpv.[i] <- vxs.[i] * vxs.[i] + vys.[i] * vys.[i] + vzs.[i] * vzs.[i]
            i <- i + NativeArray.SIMD.stride<N>
        while i < xs.Length do
            dp.[i] <- xs.[i] * xs.[i] + ys.[i] * ys.[i] + zs.[i] * zs.[i]
            i <- i + 1L
    dp    

let map3Prealloc (xs: NativeArray<N>) (ys: NativeArray<N>) (zs: NativeArray<N>) (dp: NativeArray<N>) =    
    let inline l2norm2 x y z = x*x + y*y + z*z
    for j = 1 to reps do
        NativeArray.SIMD.map3Into l2norm2 l2norm2 dp xs ys zs |> ignore
    dp

[<EntryPoint>]
let main argv = 
    // Goal: compute squared norms ||v||² = x² + y² + z²
    // of vectors v_i = [x_i, y_i, z_i]

    let cases = [("manual_prealloc", manualPrealloc)
                 ("map3_prealloc", map3Prealloc)]
    
    printfn "Initializing ..."
    use xs = NativeArray.init veclen (fun i -> scale * float32 i)
    use ys = NativeArray.init veclen (fun i -> scale * float32 (veclen - i))
    use zs = NativeArray.init veclen (fun i -> scale * float32 i)    
    use dp = NativeArray.SIMD.zeroCreate<N> veclen   
     
    printfn "Running benchmarks ..."
    for (name, f) in cases do
        timeit name (fun () -> f xs ys zs dp)

    0 // return an integer exit code
