﻿namespace NativeInteropEx

open System
open System.Numerics
open System.Runtime.CompilerServices

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
[<RequireQualifiedAccess>]
module NativeArray =
    module SIMD =
        /// Number of elements of type T in a SIMD vector
        let stride<'T when 'T: unmanaged and 'T: struct and 'T :> ValueType and 'T: (new: unit -> 'T)> = Vector<'T>.Count |> int64

        /// Map the elements of a NativeArray into an existing array
        let inline mapiInto fvec f (out: NativeArray<'U>) (xs: NativeArray<'T>) =
            if stride<'T> <> stride<'U> then
                failwith "Violated constraint: stride<'T> == stride<'U>"
            let mutable i = 0L
            let vxs = xs |> VectorIndexer<'T>
            let mutable vout = out |> VectorIndexer<'U>            
            while i < out.Length - stride<'T> do    
                vout.[i] <- fvec i vxs.[i]
                i <- i + stride<'T>
            // map tail using scalar function f
            while i < out.Length do
                out.[i] <- f i xs.[i]
                i <- i + 1L
            out

        /// Map the elements of a NativeArray into itself
        let inline mapiInplace fvec f (xs: NativeArray<'T>) =
            xs |> mapiInto fvec f xs

        /// Map the elements into a new array
        let inline mapi fvec f (xs: NativeArray<'T>) =
            let ys = NativeArray.length xs |> NativeArray<'U>
            xs |> mapiInto fvec f ys
            
        /// Map the elements of a NativeArray into an existing array
        let inline mapInto fvec f (out: NativeArray<'U>) (xs: NativeArray<'T>) =
            if stride<'T> <> stride<'U> then
                failwith "Violated constraint: stride<'T> == stride<'U>"
            let mutable i = 0L
            let vxs = xs |> VectorIndexer<'T>
            let mutable vout = out |> VectorIndexer<'U>
            while i < out.Length - stride<'T> do    
                vout.[i] <- fvec vxs.[i]
                i <- i + stride<'T>
            // map tail using scalar function f
            while i < out.Length do
                out.[i] <- f xs.[i]
                i <- i + 1L
            out

        /// Map the elements of a NativeArray into itself
        let inline mapInplace fvec f (xs: NativeArray<'T>) =
            xs |> mapInto fvec f xs

        /// Map the elements into a new array
        let inline map fvec f (xs: NativeArray<'T>) =
            let ys = NativeArray.length xs |> NativeArray<'U>
            xs |> mapInto fvec f ys

        /// Map pairwise elements of two NativeArrays into an existing output array
        let inline map2Into fvec f (out: NativeArray<'V>) (xs: NativeArray<'T>) (ys: NativeArray<'U>) =
            if stride<'T> <> stride<'U> || stride<'T> <> stride<'V> then
                failwith "Violated constraint: stride<'T> == stride<'U> == stride<'V>"
            let mutable i = 0L
            let vxs = xs |> VectorIndexer<'T>
            let vys = ys |> VectorIndexer<'U>
            let mutable vout = out |> VectorIndexer<'V>
            while i < out.Length - stride<'T> do    
                vout.[i] <- fvec vxs.[i] vys.[i]
                i <- i + stride<'T>
            // map tail using scalar function f
            while i < out.Length do
                out.[i] <- f xs.[i] ys.[i]
                i <- i + 1L
            out

        /// Map pairwise elements of two NativeArrays into an the first input array
        let inline map2Inplace fvec f (xs: NativeArray<'T>) (ys: NativeArray<'U>) =
            map2Into fvec f xs xs ys

        /// Map pairwise elements of two NativeArrays
        let inline map2 fvec f (xs: NativeArray<'T>) (ys: NativeArray<'U>) =
            let zs = NativeArray.length xs |> NativeArray<'V>
            map2Into fvec f zs xs ys

        /// Map triple-wise elements of three NativeArrays into an existing output array
        let inline map3Into fvec f (out: NativeArray<'W>) (xs: NativeArray<'T>) (ys: NativeArray<'U>) (zs: NativeArray<'V>) =
            if stride<'T> <> stride<'U> || stride<'T> <> stride<'V> || stride<'T> <> stride<'W> then
                failwith "Violated constraint: stride<'T> == stride<'U> == stride<'V> == stide<'W>"
            let mutable i = 0L
            let vxs = xs |> VectorIndexer<'T>
            let vys = ys |> VectorIndexer<'U>
            let vzs = zs |> VectorIndexer<'V>
            let mutable vout = out |> VectorIndexer<'W>
            while i < out.Length - stride<'T> do    
                vout.[i] <- fvec vxs.[i] vys.[i] vzs.[i]
                i <- i + stride<'T>
            // map tail using scalar function f
            while i < out.Length do
                out.[i] <- f xs.[i] ys.[i] zs.[i]
                i <- i + 1L
            out

        /// Map triple-wise elements of three NativeArrays into the first array
        let inline map3Inplace fvec f (xs: NativeArray<'T>) (ys: NativeArray<'U>) (zs: NativeArray<'V>) =
            map3Into fvec f xs xs ys zs
        
        /// Map triple-wise elements of three NativeArrays
        let inline map3 fvec f (xs: NativeArray<'T>) (ys: NativeArray<'U>) (zs: NativeArray<'V>) =
            let ts = NativeArray.length xs |> NativeArray<'W>
            map3Into fvec f ts xs ys zs

        /// Applies a function to each element of the array, threading a accumulator
        /// element through the computation
        let inline fold fvec f accumulate (initialState: 'State) (xs: NativeArray<'T>) = 
            let vxs = VectorIndexer<_>(xs)
            let mutable vacc = Vector<'State>(initialState)
            let mutable i = 0L
            while i < xs.Length - stride<'T> do
                vacc <- fvec vacc vxs.[i]
                i <- i + stride<'T>
            // reduce vector accumulator to scalar
            let mutable acc = vacc.[0]
            for j = 1 to int stride<'T> - 1 do
                acc <- accumulate acc vacc.[j]
            // reduce tail using scalar function f
            while i < xs.Length do
                acc <- f acc xs.[i]
                i <- i + 1L     
            acc

        /// Combines all elements of the input array into an accumulated value
        let inline reduce fvec f (xs: NativeArray<'T>) = 
            if xs.Length < 1L then
                failwith "Cannot reduce array of size 0"
            let vxs = VectorIndexer<_>(xs)
            let mutable vacc = vxs.[0]
            let mutable i = stride<'T>
            while i < xs.Length - stride<'T> do
                vacc <- fvec vacc vxs.[i]
                i <- i + stride<'T>
            // reduce vector accumulator to scalar
            let mutable acc = vacc.[0]
            for j = 1 to int stride<'T> - 1 do
                acc <- f acc vacc.[j]
            // reduce tail using scalar function f
            while i < xs.Length do
                acc <- f acc xs.[i]
                i <- i + 1L     
            acc

        /// Initializes a new NativeArray<_> of the given length (item count)
        /// by calling a generator function on each index [0 .. count]
        let inline init64 (length: int64) fvec f =
            let xs = new NativeArray<'T>(length)
            let mutable vxs = xs |> VectorIndexer<_>
            let mutable i = 0L
            while i < xs.Length - stride<'T> do
                vxs.[i] <- fvec i
                i <- i + stride<'T>
            while i < xs.Length do
                xs.[i] <- f i
                i <- i + 1L 
            xs

        /// Initializes a new NativeArray<_> of the given length (item count)
        /// by calling a generator function on each index [0 .. count]
        let inline init (length: int) fvec f: NativeArray<'T> =
            init64 (int64 length) (fun i -> i |> int |> fvec) (fun i -> i |> int |> f)

        /// Creates a NativeArray<'T> with all elements set to a given value    
        let replicate64 (length: int64) (value: 'T) =
            let xs = new NativeArray<'T>(length)
            let mutable vxs = xs |> VectorIndexer<_>
            let mutable i = 0L
            while i < xs.Length - stride<'T> do
                vxs.[i] <- Vector<'T>(value)
                i <- i + stride<'T>
            while i < xs.Length do
                xs.[i] <- value
                i <- i + 1L 
            xs 

        /// Creates a NativeArray<'T> with all elements set to a given value    
        let replicate (length: int) (value: 'T) =
            replicate64 (int64 length) value

        /// Creates a NativeArray<'T> with all elements set to the default value of 'T
        let zeroCreate64<'T when 'T: unmanaged and 'T: struct and 'T :> ValueType and 'T: (new: unit -> 'T)> (length: int64) =
            replicate64 length Unchecked.defaultof<'T>

        /// Creates a NativeArray<'T> with all elements set to the default value of 'T
        let zeroCreate<'T when 'T: unmanaged and 'T: struct and 'T :> ValueType and 'T: (new: unit -> 'T)> (length: int) =
            replicate length Unchecked.defaultof<'T>

        /// Sums the elements of a NativeArray
        let inline sum (xs: NativeArray<'T>) = xs |> reduce (+) (+)

        /// Computes the product of elements in the array
        let inline prod (xs: NativeArray<'T>) = xs |> reduce (*) (*)
                
        /// Applies a function to each paired elements of the two input arrays, threading a accumulator
        /// element through the computation
        let inline fold2 fvec f accumulate (initialState: 'State) (xs: NativeArray<'T>) (ys: NativeArray<'U>) =
            let vxs = VectorIndexer<_>(xs)
            let vys = VectorIndexer<_>(ys)
            let mutable vacc = Vector<'State>(initialState)
            let mutable i = 0L
            while i < xs.Length - stride<'T> do
                vacc <- fvec vacc vxs.[i] vys.[i]
                i <- i + stride<'T>
            // reduce vector accumulator to scalar
            let mutable acc = vacc.[0]
            for j = 1 to int stride<'T> - 1 do
                acc <- accumulate acc vacc.[j]
            // reduce tail using scalar function f
            while i < xs.Length do
                acc <- f acc xs.[i] ys.[i]
                i <- i + 1L     
            acc

        /// Computes the dot product of two arrays
        let inline dot (xs: NativeArray<'T>) (ys: NativeArray<'T>) = 
            let inline mac acc x y = acc + x * y
            fold2 mac mac (+) LanguagePrimitives.GenericZero xs ys
            
/// Helper struct to disambiguate extension method calls on NativeArray<T>
/// between the scalar or the vectorized implementations
[<Struct>]
type NativeArraySIMD<'T when 'T: unmanaged and 'T: struct and 'T: (new: unit -> 'T) and 'T :> ValueType> = 
    val array: NativeArray<'T>
    new (array: NativeArray<'T>) = { array = array }
    static member Stride = NativeArray.SIMD.stride<'T>

[<AutoOpen>]
module private Utils =
    let inline vectorize x = x |> NativeArraySIMD<_>
    let inline scalarify (x: NativeArraySIMD<_>) = x.array

[<Sealed; AbstractClass; Extension>]
type NativeArraySIMD private () =      
    /// <summary>
    /// Switch to SIMD processing of the given array
    /// </summary>
    /// <param name="array">Array to be processed in SIMD chunks (i.e. vectorized)</param>
    [<Extension>]
    static member SIMD (array: NativeArray<'T>) = array |> vectorize

    /// <summary>
    /// Switch to scalar processing of the given array
    /// </summary>
    /// <param name="array">Array to be processed in a scalar fashion (i.e. de-vectorized)</param>
    [<Extension>]
    static member ToScalar (array: NativeArraySIMD<'T>) = array.array

    /// <summary>
    /// Initializes a new array by mapping indices to initial values
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    /// <param name="fvec">Function mapping each n-th index to a vector of initial values</param>
    /// <param name="f">Function mapping the remaining length &#37; n indices to initial values</param>
    /// <remarks>n is the number of elements of type T in a SIMD vector</remarks>
    static member Initialize (length: int, fvec: Func<int, Vector<'T>>, f: Func<int, 'T>) =
        NativeArray.SIMD.init length fvec.Invoke f.Invoke |> vectorize

    /// <summary>
    /// Initializes a new array by mapping indices to initial values
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    /// <param name="fvec">Function mapping each n-th index to a vector of initial values</param>
    /// <param name="f">Function mapping the remaining length &#37; n indices to initial values</param>
    /// <remarks>n is the number of elements of type T in a SIMD vector</remarks>
    static member Initialize (length: int64, fvec: Func<int64, Vector<'T>>, f: Func<int64, 'T>) =
        NativeArray.SIMD.init64 length fvec.Invoke f.Invoke |> vectorize

    /// <summary>
    /// Creates a new array with all elements initialized to their default value
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    static member ZeroCreate (length: int) =
        NativeArray.SIMD.zeroCreate length |> vectorize

    /// <summary>
    /// Creates a new array with all elements initialized to their default value
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    static member ZeroCreate (length: int64) =
        NativeArray.SIMD.zeroCreate64 length |> vectorize

    /// <summary>
    /// Creates a new array with all elements initialized to a given value
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    /// <param name="value">Initial value of all elements</param>
    static member Replicate (length: int, value: 'T) =
        NativeArray.SIMD.replicate length value |> vectorize

    /// <summary>
    /// Creates a new array with all elements initialized to a given value
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    /// <param name="value">Initial value of all elements</param>
    static member Replicate (length: int64, value: 'T) =
        NativeArray.SIMD.replicate64 length value |> vectorize

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<float>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<float32>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<int>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<int64>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<uint32>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<uint64>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<byte>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<sbyte>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<int16>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Sums up the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative add operator</remarks>
    [<Extension>]
    static member Sum (this: NativeArraySIMD<uint16>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<float>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<float32>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<int>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<int64>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<uint32>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<uint64>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<byte>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<sbyte>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<int16>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the product of the elements of an array
    /// </summary>
    /// <param name="this">Array to be summed</param>
    /// <remarks>Assumes commutative multiply operator</remarks>
    [<Extension>]
    static member Product (this: NativeArraySIMD<uint16>) =
        this |> scalarify |> NativeArray.SIMD.sum

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<float>, other: NativeArray<float>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<float32>, other: NativeArray<float32>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<int>, other: NativeArray<int>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<int64>, other: NativeArray<int64>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<uint32>, other: NativeArray<uint32>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<uint64>, other: NativeArray<uint64>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<byte>, other: NativeArray<byte>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<sbyte>, other: NativeArray<sbyte>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<int16>, other: NativeArray<int16>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Computes the dot product of two arrays
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <remarks>Assumes commutative multiply and add operators</remarks>
    [<Extension>]
    static member Dot (this: NativeArraySIMD<uint16>, other: NativeArray<uint16>) =
        NativeArray.SIMD.dot this.array other

    /// <summary>
    /// Maps the elements of an array to new values
    /// </summary>
    /// <param name="this">Array containing the elements to be mapped</param>
    /// <param name="fvec">Function mapping a vector of n values to a vector of n output values</param>
    /// <param name="f">Function mapping a value to an output value</param>
    [<Extension>]
    static member Map (this: NativeArraySIMD<'T>, fvec: Func<Vector<'T>, Vector<'U>>, f: Func<'T, 'U>): NativeArraySIMD<'U> =
        this |> scalarify |> NativeArray.SIMD.map fvec.Invoke f.Invoke |> vectorize
    
    /// <summary>
    /// Maps the elements of an array to new values
    /// </summary>
    /// <param name="this">Array containing the elements to be mapped</param>
    /// <param name="fvec">Function mapping a vector of n values to a vector of n output values</param>
    /// <param name="f">Function mapping a value to an output value</param>
    /// <param name="out">Output array where to store the mapped values</param>
    [<Extension>]
    static member Map (this: NativeArraySIMD<'T>, fvec: Func<Vector<'T>, Vector<'U>>, f: Func<'T, 'U>, out: NativeArray<'U>): NativeArraySIMD<'U> =
        this |> scalarify |> NativeArray.SIMD.mapInto fvec.Invoke f.Invoke out |> vectorize    

    /// <summary>
    /// Maps the elements of an array to new values
    /// </summary>
    /// <param name="this">Array containing the elements to be mapped</param>
    /// <param name="fvec">Function mapping a vector of n values and the index of the first of those elements to a vector of n output values</param>
    /// <param name="f">Function mapping a value and its corresponding index to an output value</param>
    [<Extension>]
    static member Map (this: NativeArraySIMD<'T>, fvec: Func<int64, Vector<'T>, Vector<'U>>, f: Func<int64, 'T, 'U>): NativeArraySIMD<'U> =
        this |> scalarify |> NativeArray.SIMD.mapi (fun i x -> fvec.Invoke(i, x)) (fun i x -> f.Invoke(i, x)) |> vectorize

    /// <summary>
    /// Maps the elements of an array to new values
    /// </summary>
    /// <param name="this">Array containing the elements to be mapped</param>
    /// <param name="fvec">Function mapping a vector of n values and the index of the first of those elements to a vector of n output values</param>
    /// <param name="f">Function mapping a value and its corresponding index to an output value</param>
    /// <param name="out">Output array where to store the mapped values</param>
    [<Extension>]
    static member Map (this: NativeArraySIMD<'T>, fvec: Func<int64, Vector<'T>, Vector<'U>>, f: Func<int64, 'T, 'U>, out: NativeArray<'U>): NativeArraySIMD<'U> =
        this |> scalarify |> NativeArray.SIMD.mapiInto (fun i x -> fvec.Invoke(i, x)) (fun i x -> f.Invoke(i, x)) out |> vectorize
    
    /// <summary>
    /// Maps the elements of an array to new values, overriding the original values of the array
    /// </summary>
    /// <param name="this">Array containing the elements to be mapped</param>
    /// <param name="fvec">Function mapping a vector of n values to a vector of n output values</param>
    /// <param name="f">Function mapping a value to an output value</param>
    [<Extension>]
    static member MapInplace (this: NativeArraySIMD<'T>, fvec: Func<Vector<'T>, Vector<'T>>, f: Func<'T, 'T>): NativeArraySIMD<'T> =
        this |> scalarify |> NativeArray.SIMD.mapInplace fvec.Invoke f.Invoke |> vectorize
    
    /// <summary>
    /// Maps the elements of an array to new values, overriding the original values of the array
    /// </summary>
    /// <param name="this">Array containing the elements to be mapped</param>
    /// <param name="fvec">Function mapping a vector of n values and the index of the first of those elements to a vector of n output values</param>
    /// <param name="f">Function mapping a value and its corresponding index to an output value</param>
    [<Extension>]
    static member MapInplace (this: NativeArraySIMD<'T>, fvec: Func<int64, Vector<'T>, Vector<'T>>, f: Func<int64, 'T, 'T>): NativeArraySIMD<'T> =
        this |> scalarify |> NativeArray.SIMD.mapiInplace (fun i x -> fvec.Invoke(i, x)) (fun i x -> f.Invoke(i, x)) |> vectorize
    
    /// <summary>
    /// Maps the paired elements of an array to new values
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <param name="fvec">Function mapping two vectors of n values each to a vector of n output values</param>
    /// <param name="f">Function mapping two input values to an output value</param>
    [<Extension>]
    static member Map (this: NativeArraySIMD<'T>, other: NativeArray<'U>, fvec: Func<Vector<'T>, Vector<'U>, Vector<'V>>, f: Func<'T, 'U, 'V>): NativeArraySIMD<'V> =
        NativeArray.SIMD.map2 (fun x y -> fvec.Invoke(x,y)) (fun x y -> f.Invoke(x,y)) this.array other |> vectorize

    /// <summary>
    /// Maps the paired elements of an array to new values
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <param name="fvec">Function mapping two vectors of n values each to a vector of n output values</param>
    /// <param name="f">Function mapping two input values to an output value</param>
    /// <param name="out">Array to store the results in</param>
    [<Extension>]
    static member Map (this: NativeArraySIMD<'T>, other: NativeArray<'U>, fvec: Func<Vector<'T>, Vector<'U>, Vector<'V>>, f: Func<'T, 'U, 'V>, out: NativeArray<'V>): NativeArraySIMD<'V> =
        NativeArray.SIMD.map2Into (fun x y -> fvec.Invoke(x,y)) (fun x y -> f.Invoke(x,y)) out this.array other |> vectorize

    /// <summary>
    /// Maps the paired elements of an array to new values, overriding the first array
    /// </summary>
    /// <param name="this">First operand</param>
    /// <param name="other">Second operand</param>
    /// <param name="fvec">Function mapping two vectors of n values each to a vector of n output values</param>
    /// <param name="f">Function mapping two input values to an output value</param>
    [<Extension>]
    static member MapInplace (this: NativeArraySIMD<'T>, other: NativeArray<'U>, fvec: Func<Vector<'T>, Vector<'U>, Vector<'T>>, f: Func<'T, 'U, 'T>): NativeArraySIMD<'T> =
        NativeArray.SIMD.map2Inplace (fun x y -> fvec.Invoke(x,y)) (fun x y -> f.Invoke(x,y)) this.array other |> vectorize

    /// <summary>
    /// Reduces an array to a single value
    /// </summary>
    /// <param name="this">Array to be reduced</param>
    /// <param name="fvec">Function to combine two vectors of n values into one vector of n values</param>
    /// <param name="f">Function to combine two scalar values into one</param>
    /// <remarks>Assumes that both fvec and f are commutative operations</remarks>
    [<Extension>]
    static member Reduce (this: NativeArraySIMD<'T>, fvec: Func<Vector<'T>, Vector<'T>, Vector<'T>>, f: Func<'T, 'T, 'T>): 'T =
        this |> scalarify |> NativeArray.SIMD.reduce (fun a x -> fvec.Invoke(a, x)) (fun a x -> f.Invoke(a, x))

    /// <summary>
    /// Applies a function to each element of the array, threading a accumulator
    /// element through the computation
    /// </summary>
    /// <param name="fvec">Function to combine two vectors of n values into one vector of n values</param>
    /// <param name="f">Function to combine two scalar values into one</param>
    /// <param name="accumulate">Function to accumulate the vectorized state created by fvec</param>
    /// <param name="state">Initial value of the accumulator</param>
    /// <remarks>Assumes that both fvec and f are commutative operations</remarks>
    [<Extension>]
    static member Fold (this: NativeArraySIMD<'T>, 
                        fvec: Func<Vector<'State>, Vector<'T>, Vector<'State>>, 
                        f: Func<'State, 'T, 'State>, 
                        accumulate: Func<'State, 'State, 'State>, 
                        state: 'State) =
        this |> scalarify |> NativeArray.SIMD.fold (fun s x -> fvec.Invoke(s, x)) (fun s x -> f.Invoke(s, x)) (fun a b -> accumulate.Invoke(a, b)) state

    /// <summary>
    /// Applies a function to each pair of elements of two arrays, threading a accumulator
    /// element through the computation
    /// </summary>
    /// <param name="fvec">Function to combine the current vectorized state and two vectors of n values into one vector of n values</param>
    /// <param name="f">Function to combine the current scalar state and two scalar values into one</param>
    /// <param name="accumulate">Function to accumulate the vectorized state created by fvec</param>
    /// <param name="state">Initial value of the accumulator</param>
    /// <remarks>Assumes that both fvec and f are commutative operations</remarks>
    [<Extension>]
    static member Fold (this: NativeArraySIMD<'T>, 
                        other: NativeArray<'U>, 
                        fvec: Func<Vector<'State>, Vector<'T>, Vector<'U>, Vector<'State>>, 
                        f: Func<'State, 'T, 'U, 'State>, 
                        accumulate: Func<'State, 'State, 'State>, 
                        state: 'State): 'State =
        NativeArray.SIMD.fold2 (fun a x y -> fvec.Invoke(a, x, y)) (fun a x y -> f.Invoke(a, x, y)) (fun a b -> accumulate.Invoke(a, b)) state this.array other
        
