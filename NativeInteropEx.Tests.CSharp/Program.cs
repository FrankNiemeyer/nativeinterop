﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;

namespace NativeInteropEx.Tests.CSharp
{
    class Program
    {
        [StructLayout(LayoutKind.Sequential)]
        struct MyLittleStruct
        {
            public int i;
            public float f;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct MyBigStruct
        {
            public ulong i1;
            public ulong i2;
        }

        static unsafe void ReadWriteUnsafeBuffer() {
            var mls = new MyLittleStruct { i = 123, f = 4.5f };
            var buff = stackalloc byte[sizeof(MyLittleStruct)];
            var p = new IntPtr(buff);
            // store mls in unmanaged buffer buff
            p.Write(mls);
            // restore struct from stack allocated buffer
            var mlsCopy = p.Read<MyLittleStruct>();
            
            Console.WriteLine("Write/read struct to/frome unsafe buffer:");
            Console.WriteLine(mls.i == mlsCopy.i);
            Console.WriteLine(mls.f == mlsCopy.f);           
        }

        static unsafe void ReadWriteUnsafeBufferGeneric<T>(T value) where T : struct {
            var buff = stackalloc byte[Marshal.SizeOf<T>()];
            //var tbuff = (T*)buff; // does not compile; not possible in C#
            //*tbuff = value;
            //var valueCopy = *tbuff;
            
            // solution using NativeInteropEx:
            var tbuff = new IntPtr(buff);
            // store value in unmanaged buffer buff
            tbuff.Write(value);
            // restore struct from stack allocated buffer
            var valueCopy = tbuff.Read<T>();
        }

        static void Main(string[] args) {
            var iii = 10;
            Console.WriteLine($"Old {iii}");
            Structure.Write(ref iii, 1.23f);
            Console.WriteLine($"New {iii}");
            Console.WriteLine($"New as float {Structure.Read<int, float>(ref iii)}");

            var sourceInts = new int[] { 192, 168, 3 }; // 4 ints = 12 bytes
            var intsAsBytes = Buffer.Copy<int, byte>(sourceInts);
            var intsAsBytesAsInts = Buffer.Copy<byte, int>(intsAsBytes);
            
            // convert array long -> byte via memcpy
            var sourceLongs = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var longsAsBytes = Buffer.Copy<long, byte>(sourceLongs);
            var longsAsBytesAsLongs = Buffer.Copy<byte, long>(longsAsBytes);
            
            // instead of copying, *reinterpret* sourceLongs as bytes
            var handle = GCHandle.Alloc(sourceLongs, GCHandleType.Pinned);
            var plongs = handle.AddrOfPinnedObject();
            var longByte0 = plongs.Get<byte>(0);
            var longByte1 = plongs.Get<byte>(1);
            Console.WriteLine("{0}, {1}", longByte0, longByte1);
            handle.Free();
            
            // same, but using ReinterpretCast
            using (var byteView = new ReinterpretCast<long[], byte>(sourceLongs)) {
                var longByte0a = byteView[0];
                var longByte1a = byteView[1];
                Console.WriteLine("{0}, {1}", longByte0a, longByte1a);
            }

            var sourceBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            var bytesAsBytes = Buffer.Copy<byte, byte>(sourceBytes);

            var longFrom4567bytes = Structure.FromBuffer<byte, long>(sourceBytes, 3);
            
            var sourceStruct = new MyLittleStruct { i = 13, f = -1.3f };
            var structAsUlong = Structure.Read<MyLittleStruct, ulong>(ref sourceStruct);
            var structFromUlong = Structure.Read<ulong, MyLittleStruct>(ref structAsUlong);
            
            var bigSource = new MyBigStruct { i1 = 634, i2 = 8494 };
            var bigSourceBytes = Structure.ToBuffer<MyBigStruct, byte>(bigSource);
            var bigSourceFromBytes = Structure.FromBuffer<byte, MyBigStruct>(bigSourceBytes);
            var littleFromBig = Structure.ToBuffer<MyBigStruct, MyLittleStruct>(bigSource);
            var someIntermediateInt = Structure.FromBuffer<byte, int>(bigSourceBytes, 4);
            Console.WriteLine(someIntermediateInt);
            
            // read the first four bytes of bigSource as individual bytes
            var b1 = Structure.Get<MyBigStruct, byte>(ref bigSource, 0);
            var b2 = Structure.Get<MyBigStruct, byte>(ref bigSource, 1);
            var b3 = Structure.Get<MyBigStruct, byte>(ref bigSource, 2);
            var b4 = Structure.Get<MyBigStruct, byte>(ref bigSource, 3);
            // compare three methods to get to the individual bytes of bigSource
            Console.WriteLine($"Structure.Get:      {b1}:{b2}:{b3}:{b4}");
            Console.WriteLine($"Structure.ToBuffer: {bigSourceBytes[0]}:{bigSourceBytes[1]}:{bigSourceBytes[2]}:{bigSourceBytes[3]}");
            using (var byteView = new ReinterpretCast<MyBigStruct, byte>(bigSource)) {
                Console.WriteLine($"ReinterpretCast:    {byteView[0]}:{byteView[1]}:{byteView[2]}:{byteView[3]}");
            }
            
            // create new structure and retrieve
            var mls = new MyLittleStruct { i = 10, f = 0.3f };
            var mlsHandle = GCHandle.Alloc(mls, GCHandleType.Pinned);
            // read binary structured data from pointer
            var mlsCopy = mlsHandle.AddrOfPinnedObject().Read<MyLittleStruct>();
            // release GC handle
            mlsHandle.Free();
            
            Console.WriteLine(mlsCopy.i);
            Console.WriteLine(mlsCopy.f);
            
            var mlsBuffer = Structure.ToBuffer<MyLittleStruct, byte>(mls);
            Console.WriteLine("Buffer size: {0}", mlsBuffer.Length);
            var mls2 = Structure.FromBuffer<byte, MyLittleStruct>(mlsBuffer);
            Console.WriteLine("Deserialized mls i: {0}", mls2.i);
            Console.WriteLine("Deserialized mls f: {0}", mls2.f);

            ReadWriteUnsafeBuffer();
            
            // "memcpy": Copy the bytes of an int[] to a byte[]
            var ints = new[] { 1, 1024, 2048, 4096 };
            var intBytes = Buffer.Copy<int, byte>(ints);
            // little endian -> LSB on lowest addr.
            Console.WriteLine("   1 bytes: {3}.{2}.{1}.{0}", intBytes[0], intBytes[1], intBytes[2], intBytes[3]);
            Console.WriteLine("1024 bytes: {3}.{2}.{1}.{0}", intBytes[4], intBytes[5], intBytes[6], intBytes[7]);
            Console.WriteLine("2048 bytes: {3}.{2}.{1}.{0}", intBytes[8], intBytes[9], intBytes[10], intBytes[11]);
            Console.WriteLine("4096 bytes: {3}.{2}.{1}.{0}", intBytes[12], intBytes[13], intBytes[14], intBytes[15]);
            var ints2 = Buffer.Copy<byte, int>(intBytes);
            Console.WriteLine("ints: {0}, {1}, {2}, {3}", ints2[0], ints2[1], ints2[2], ints2[3]);
            
            using (var narr = NativeArray.Initialize(10, i => i * 2.0f)) {
                // output some diagnostics
                Console.WriteLine($"Allocated {narr.AllocatedBytes} bytes");
                Console.WriteLine($"Base address: {narr.BaseAddress:X}");
                
                // enumerate array
                var sum = 0.0f;
                foreach (var n in narr)
                    sum += n;
                Console.WriteLine($"Summed vals [foreach] {sum}");
                sum = 0.0f;
                for (var i = 0; i < narr.Length; ++i)
                    sum += narr[i];
                Console.WriteLine($"Summed vals [for] {sum}");
                Console.WriteLine($"Summed vals [ExtSum] {narr.Sum()}");
                
                // write to native array
                narr[0] = 1.0f;
                narr[1] = 2.0f;
                narr[2] = 3.0f;
                var x = narr[0] + narr[1];
                Console.WriteLine(x);
                // note: indexer does not check for out-of-bound index,
                // so this may "work", but return arbitrary garbage
                var invalidIndex = narr[narr.Length];

                // use At method to force bounds checks
                narr.Set(0, 1.0f);
                
                var x2 = narr.Get(0) + narr.Get(1);
                Console.WriteLine(x2);
                //narr.At(narr.Length); // will throw IndexOutOfRange

                // using extension methods for IntPtr
                var m3 = narr.BaseAddress.Read<float>();
                var m4 = narr.BaseAddress.Read<int>();
                Console.WriteLine($"{m3} :: float == {m4} :: int");
                
                // reinterpreting narr's memory as an int array
                var pNarrI = new NativePtr<int>(narr.BaseAddress);
                var pn1I = pNarrI.Value; // narr[0]
                var pn2I = (pNarrI + 1).Value; // narr[1];
                var pn3I = pNarrI[1]; // narr[2]
                Console.WriteLine(pn1I);
                Console.WriteLine(pn2I);
                Console.WriteLine(pn3I);            
            }

            // test array arithmetic
            using (var xs = NativeArray.Initialize(50, i => i * 2.0f))
            using (var ys = NativeArray.Initialize(50, i => (50 - i) * 2.0f)) {
                var dp = xs.Dot(ys);
                Console.WriteLine($"scalar <xs, ys> = {dp}");
                
                var dpSimd = xs.SIMD().Dot(ys);
                Console.WriteLine($"   vec <xs, ys> = {dpSimd}");

                var filteredXs = xs.Where(x => x % 2.0f == 0.0f).ToNativeArray();
                Console.WriteLine($"filtered xs = {filteredXs}");
            }      
        }
    }
}
