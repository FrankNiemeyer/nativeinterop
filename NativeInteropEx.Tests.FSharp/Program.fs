﻿#nowarn "9"
open System.Runtime.InteropServices
open NativeInteropEx
open System.Numerics

[<EntryPoint>]
let main argv = 
    // create native array by copying a managed array
    let managedarray = [| 0; 1; 2; 3; 4 |]
    let nativearray = managedarray |> NativeArray.ofArray
    nativearray |> Seq.iteri (fun i x -> printfn "a[%i] = %o" i x)        
    printfn "NativeArray<int>   = %O" nativearray
    let narr2 = nativearray |> NativeArray.copy
    printfn "NativeArray<int>   = %O" narr2
    let narr3: NativeArray<uint8> = nativearray |> NativeArray.convert
    printfn "NativeArray<uint8> = %O" narr3    
    
    // filter array
    let filteredNarr = nativearray |> NativeArray.filter (fun x -> x % 2 = 0)
    printfn "filtered = %O" filteredNarr    

    // allocate zero-length array
    let arr: NativeArray<byte> = NativeArray.zeroCreate 0
    // resize to 10 bytes
    let arrRes = arr |> NativeArray.resize 10
    arrRes.At(9) <- 12uy
    printfn "new length = %i" arrRes.Length
    arr.Dispose ()
    arrRes.Dispose ()    

    // test vectorized array ops
    let v1 = NativeArray.SIMD.replicate 10 1.23
    printfn "v1 = %O" v1
    let v2 = v1 |> NativeArray.SIMD.map (fun x -> x + x) (fun x -> x + x)
    printfn "v2 = %O" v2
    
    let res = 
        NativeArray.SIMD.init 1000000 (float >> Vector<_>) float
        |> NativeArray.SIMD.mapInplace (fun v -> v + v) (fun s -> s + s) 
        |> NativeArray.SIMD.mapInplace Vector.SquareRoot sqrt
        |> NativeArray.SIMD.sum
    printfn "res = %A" res
    
    let xs = NativeArray.init 10 float32
    let ys = NativeArray.init 10 ((*)2 >> float32)
    let norm2 = NativeArray.SIMD.map2 (*) (*) xs ys
                |> NativeArray.SIMD.reduce (+) (+)       
    printfn "map2 >> reduce = %A" norm2
    let vdp = NativeArray.SIMD.dot xs ys        
    printfn "SIMD dp = %A" vdp
    let dp = NativeArray.dot xs ys        
    printfn "Scalar dp = %A" dp
    
    let ms = ys |> Seq.filter (fun x -> x % 2.0f = 0.0f) |> NativeArray.ofSeq
    printfn "filtered ys = %O" ms

    // approach for reference types
    // ReinterpretCast pins object and provides a typed pointer
    let longArray = [|1L; 2L; 3L;|]
    use byteView = new ReinterpretCast<_, byte>(longArray)
    let byte0 = byteView.[0];
    byteView.[0] <- byte0
    // recommended F# approach for unmanaged types:
    // use nativeptr to stack value directly
    let mutable longval = 3L  
    let mutable ptr = &&longval |> NativePtr.cast |> NativePtr<byte>
    let byte0 = ptr.[0]
    ptr.[0] <- byte0

    // other option
    let byte0: byte = NativeInteropEx.Structure.get &longval 0    

    0
