﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Runtime.InteropServices;

namespace NativeInterop.UnitTests
{
    [TestClass]
    public class NativePtr
    {
        [StructLayout(LayoutKind.Sequential)]
        struct FloatIntStruct
        {
            public float a;
            public int b;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct Byte4Struct
        {
            public byte a;
            public byte b;
            public byte c;
            public byte d;
        }

        [TestMethod]
        public void AddOffset32RawBytePtr() {
            var p = new IntPtr(123);
            var pPlus1278 = p.AddOffset<byte>(1278);
            Assert.AreEqual(new IntPtr(123 + 1278), pPlus1278);
        }

        [TestMethod]
        public void AddOffset32WrappedBytePtr() {
            var p = new NativePtr<byte>(new IntPtr(123));
            var pPlus1278 = p + 1278;
            Assert.AreEqual(new IntPtr(123 + 1278), pPlus1278.RawPointer);
        }

        [TestMethod]
        public void AddOffset32RawInt32Ptr() {
            var p = new IntPtr(123);
            var pPlus1278 = p.AddOffset<int>(1278);
            Assert.AreEqual(new IntPtr(123 + sizeof(int) * 1278), pPlus1278);
        }

        [TestMethod]
        public void AddOffset32WrappedInt32Ptr() {
            var p = new NativePtr<int>(new IntPtr(123));
            var pPlus1278 = p + 1278;
            Assert.AreEqual(new IntPtr(123 + sizeof(int) * 1278), pPlus1278.RawPointer);
        }

        [TestMethod]
        public void AddOffset32RawByte4Ptr() {
            var p = new IntPtr(123);
            var pPlus1278 = p.AddOffset<Byte4Struct>(1278);
            Assert.AreEqual(new IntPtr(123 + 4 * sizeof(byte) * 1278), pPlus1278);
        }

        [TestMethod]
        public void AddOffset32WrappedByte4Ptr() {
            var p = new NativePtr<Byte4Struct>(new IntPtr(123));
            var pPlus1278 = p + 1278;
            Assert.AreEqual(new IntPtr(123 + 4 * sizeof(byte) * 1278), pPlus1278.RawPointer);
        }

        [TestMethod]
        public void AddOffset64RawBytePtr() {
            var p = new IntPtr(123);
            var pPlus1278 = p.AddOffset<byte>(1278L);
            Assert.AreEqual(new IntPtr(123 + 1278L), pPlus1278);
        }

        [TestMethod]
        public void AddOffset64WrappedBytePtr() {
            var p = new NativePtr<byte>(new IntPtr(123));
            var pPlus1278 = p + 1278L;
            Assert.AreEqual(new IntPtr(123 + 1278L), pPlus1278.RawPointer);
        }

        [TestMethod]
        public void AddOffset64RawInt32Ptr() {
            var p = new IntPtr(123);
            var pPlus1278 = p.AddOffset<int>(1278L);
            Assert.AreEqual(new IntPtr(123 + sizeof(int) * 1278L), pPlus1278);
        }

        [TestMethod]
        public void AddOffset64WrappedInt32Ptr() {
            var p = new NativePtr<int>(new IntPtr(123));
            var pPlus1278 = p + 1278L;
            Assert.AreEqual(new IntPtr(123 + sizeof(int) * 1278L), pPlus1278.RawPointer);
        }

        [TestMethod]
        public void AddOffset64RawByte4Ptr() {
            var p = new IntPtr(123);
            var pPlus1278 = p.AddOffset<Byte4Struct>(1278L);
            Assert.AreEqual(new IntPtr(123 + 4 * sizeof(byte) * 1278L), pPlus1278);
        }

        [TestMethod]
        public void AddOffset64WrappedByte4Ptr() {
            var p = new NativePtr<Byte4Struct>(new IntPtr(123));
            var pPlus1278 = p + 1278L;
            Assert.AreEqual(new IntPtr(123 + 4 * sizeof(byte) * 1278L), pPlus1278.RawPointer);
        }

        [TestMethod]
        public void IncRawByte4Ptr() {
            var p = new IntPtr(123);
            var pInc = p.Increment<Byte4Struct>();
            Assert.AreEqual(new IntPtr(123 + 4 * sizeof(byte)), pInc);
        }
        
        [TestMethod]
        public void DecRawByte4Ptr() {
            var p = new IntPtr(123);
            var pDec = p.Decrement<Byte4Struct>();
            Assert.AreEqual(new IntPtr(123 - 4 * sizeof(byte)), pDec);
        }

        [TestMethod]
        public unsafe void ReadRawByte4Ptr() {
            var b4struct = new Byte4Struct { a = 1, b = 2, c = 3, d = 4 };
            var p = new IntPtr(&b4struct);
            var b4structCopy = p.Read<Byte4Struct>();
            Assert.AreEqual(b4struct, b4structCopy);
        }

        [TestMethod]
        public unsafe void ReadWrappedByte4Ptr() {
            var b4struct = new Byte4Struct { a = 1, b = 2, c = 3, d = 4 };
            var p = new NativePtr<Byte4Struct>(new IntPtr(&b4struct));
            var b4structCopy = p.Value;
            Assert.AreEqual(b4struct, b4structCopy);
        }

        [TestMethod]
        public unsafe void WriteRawByte4Ptr() {
            var b4struct = new Byte4Struct { a = 1, b = 2, c = 3, d = 4 };
            var b4structCopy = new Byte4Struct { a = 255, b = 255, c = 255, d = 255 };
            var p = new IntPtr(&b4structCopy);
            p.Write(b4struct);
            Assert.AreEqual(b4struct, b4structCopy);
        }

        [TestMethod]
        public unsafe void WriteWrappedByte4Ptr() {
            var b4struct = new Byte4Struct { a = 1, b = 2, c = 3, d = 4 };
            var b4structCopy = new Byte4Struct { a = 255, b = 255, c = 255, d = 255 };
            var p = new NativePtr<Byte4Struct>(new IntPtr(&b4structCopy));
            p.Value = b4struct;
            Assert.AreEqual(b4struct, b4structCopy);
        }

        [TestMethod]
        public unsafe void Get32RawByte4Ptr() {
            var b4Arr = new Byte4Struct[] { new Byte4Struct { a = 1, b = 2, c = 3, d = 4 },
                                            new Byte4Struct { a = 5, b = 6, c = 7, d = 8 } };
            fixed (Byte4Struct* b4ArrPtr = &b4Arr[0])
            {
                var b4ArrSecondItem = new IntPtr(b4ArrPtr).Get<Byte4Struct>(1);
                Assert.AreEqual(b4Arr[1], b4ArrSecondItem);
            }
        }

        [TestMethod]
        public unsafe void Get32WrappedByte4Ptr() {
            var b4Arr = new Byte4Struct[] { new Byte4Struct { a = 1, b = 2, c = 3, d = 4 },
                                            new Byte4Struct { a = 5, b = 6, c = 7, d = 8 } };
            fixed (Byte4Struct* b4ArrPtr = &b4Arr[0])
            {
                var p = new NativePtr<Byte4Struct>(new IntPtr(b4ArrPtr));
                var b4ArrSecondItem = p[1];
                Assert.AreEqual(b4Arr[1], b4ArrSecondItem);
            }
        }

        [TestMethod]
        public unsafe void Get64RawByte4Ptr() {
            var b4Arr = new Byte4Struct[] { new Byte4Struct { a = 1, b = 2, c = 3, d = 4 },
                                            new Byte4Struct { a = 5, b = 6, c = 7, d = 8 } };
            fixed (Byte4Struct* b4ArrPtr = &b4Arr[0])
            {
                var b4ArrSecondItem = new IntPtr(b4ArrPtr).Get<Byte4Struct>(1L);
                Assert.AreEqual(b4Arr[1], b4ArrSecondItem);
            }
        }

        [TestMethod]
        public unsafe void Get64WrappedByte4Ptr() {
            var b4Arr = new Byte4Struct[] { new Byte4Struct { a = 1, b = 2, c = 3, d = 4 },
                                            new Byte4Struct { a = 5, b = 6, c = 7, d = 8 } };
            fixed (Byte4Struct* b4ArrPtr = &b4Arr[0])
            {
                var p = new NativePtr<Byte4Struct>(new IntPtr(b4ArrPtr));
                var b4ArrSecondItem = p[1L];
                Assert.AreEqual(b4Arr[1], b4ArrSecondItem);
            }
        }

        [TestMethod]
        public unsafe void Set32RawByte4Ptr() {
            var b4Arr = new Byte4Struct[] { new Byte4Struct { a = 1, b = 2, c = 3, d = 4 },
                                            new Byte4Struct { a = 5, b = 6, c = 7, d = 8 } };
            fixed (Byte4Struct* b4ArrPtr = &b4Arr[0])
            {
                new IntPtr(b4ArrPtr).Set(1, b4Arr[0]);
                Assert.AreEqual(b4Arr[0], b4Arr[1]);
            }
        }

        [TestMethod]
        public unsafe void Set32WrappedByte4Ptr() {
            var b4Arr = new Byte4Struct[] { new Byte4Struct { a = 1, b = 2, c = 3, d = 4 },
                                            new Byte4Struct { a = 5, b = 6, c = 7, d = 8 } };
            fixed (Byte4Struct* b4ArrPtr = &b4Arr[0])
            {
                var p = new NativePtr<Byte4Struct>(new IntPtr(b4ArrPtr));
                p[1] = b4Arr[0];
                Assert.AreEqual(b4Arr[0], b4Arr[1]);
            }
        }

        [TestMethod]
        public unsafe void Set64RawByte4Ptr() {
            var b4Arr = new Byte4Struct[] { new Byte4Struct { a = 1, b = 2, c = 3, d = 4 },
                                            new Byte4Struct { a = 5, b = 6, c = 7, d = 8 } };
            fixed (Byte4Struct* b4ArrPtr = &b4Arr[0])
            {
                new IntPtr(b4ArrPtr).Set(1L, b4Arr[0]);
                Assert.AreEqual(b4Arr[0], b4Arr[1]);
            }
        }

        [TestMethod]
<<<<<<< local
        public unsafe void ReadNativePtr() {
            var b4struct = new Byte4Struct { a = 1, b = 2, c = 3, d = 4 };
            var pb4 = new IntPtr(&b4struct);
            var p = new NativePtr<Byte4Struct>(pb4);
            var b4copy = p.Value;
            Assert.AreEqual(b4struct, b4copy);
=======
        public unsafe void Set64WrappedByte4Ptr() {
            var b4Arr = new Byte4Struct[] { new Byte4Struct { a = 1, b = 2, c = 3, d = 4 },
                                            new Byte4Struct { a = 5, b = 6, c = 7, d = 8 } };
            fixed (Byte4Struct* b4ArrPtr = &b4Arr[0])
            {
                var p = new NativePtr<Byte4Struct>(new IntPtr(b4ArrPtr));
                p[1L] = b4Arr[0];
                Assert.AreEqual(b4Arr[0], b4Arr[1]);
            }
>>>>>>> other
        }
<<<<<<< local

        [TestMethod]
        public unsafe void WriteNativePtr() {
            var b4struct1 = new Byte4Struct { a = 0, b = 0, c = 0, d = 0 };
            var b4struct2 = new Byte4Struct { a = 1, b = 2, c = 3, d = 4 };
            var pb4 = new IntPtr(&b4struct1);
            var p = new NativePtr<Byte4Struct>(pb4);
            p.Value = b4struct2;
            Assert.AreEqual(b4struct2, p.Value);
        }
=======
>>>>>>> other
    }
}
