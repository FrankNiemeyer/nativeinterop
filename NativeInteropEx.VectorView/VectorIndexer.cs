﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace NativeInteropEx
{
    /// <summary>
    /// Wraps a raw memory address or a NativeArray with an indexer
    /// providing vectorized access
    /// </summary>
    public struct VectorIndexer<T> where T : struct
    {
        IntPtr pBase;

        public VectorIndexer(IntPtr baseAddress) {
            pBase = baseAddress;
        }

        public VectorIndexer(NativeArray<T> array) {
            pBase = array.BaseAddress;
        }

        public Vector<T> this[int idx]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get { return VectorView.Get<T>(pBase, idx); }
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set { VectorView.Set(pBase, idx, value); }
        }

        public Vector<T> this[long idx]
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get { return VectorView.Get<T>(pBase, idx); }
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set { VectorView.Set(pBase, idx, value); }
        }
    }
}
