﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace NativeInteropEx
{
    /// <summary>
    /// Helper class that provides access to memory in SIMD strides of 
    /// size Vector&lt;T&gt;.Count * sizeof(T) chunks from F#
    /// </summary>
    /// <remarks>Simply forwards to NativeInteropEx.IntPtrEx, in turn forwarding to
    /// NativeInterop.NativePtr, but strips off the unmanaged constraint, so that we 
    /// can treat any T* as Vector&lt;T&gt;* even from F#</remarks>
    /// <typeparam name="T">Underlying primitive type (must be supported by Vector&lt;T&gt;)</typeparam>
    public static class VectorView
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector<T> Get<T>(IntPtr p, int idx) where T : struct => p.AddOffset<T>(idx).Read<Vector<T>>();

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector<T> Get<T>(IntPtr p, long idx) where T : struct => p.AddOffset<T>(idx).Read<Vector<T>>();

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Set<T>(IntPtr p, int idx, Vector<T> value) where T : struct => p.AddOffset<T>(idx).Write(value);
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Set<T>(IntPtr p, long idx, Vector<T> value) where T : struct => p.AddOffset<T>(idx).Write(value);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector<T> Read<T>(IntPtr p) where T : struct => p.Read<Vector<T>>();

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Write<T>(IntPtr p, Vector<T> value) where T : struct => p.Write(value);        
    }
}
