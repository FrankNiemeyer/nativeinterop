﻿namespace NativeInteropEx

#nowarn "9" // may generate unverifiable IL

open System.Runtime.CompilerServices

[<RequireQualifiedAccess>]
module Buffer =
    open System
    open System.Runtime.InteropServices
                
    let inline private copySingleBlock pSrc pDst =
        pSrc |> NativePtr.read |> NativePtr.write pDst
    
    let inline private copySingleBlockOffset pSrc pDst offset =
        pSrc |> NativePtr.get offset |> NativePtr.set pDst offset

    let private memcpyBlockAligned<'T when 'T: unmanaged> pSrc pDst length =
        let sizeofT = nativeint sizeof<'T>
        let notAligned = sizeofT - (nativeint 1)
        let mutable len = length
        // copy byte-wise until aligned to sizeofT
        let mutable dst: nativeptr<uint8> = pDst |> NativePtr.ofNativeInt
        let mutable src: nativeptr<uint8> = pSrc |> NativePtr.ofNativeInt
        while len >= (nativeint 0) && ((dst |> NativePtr.toNativeInt) &&& notAligned <> (nativeint 0)) do
            copySingleBlock src dst
            src <- src |> NativePtr.inc
            dst <- dst |> NativePtr.inc
            len <- len - (nativeint 1)
        // copy aligned memory blocks
        let mutable dst: nativeptr<'T> = dst |> NativePtr.cast
        let mutable src: nativeptr<'T> = src |> NativePtr.cast
        let blocksize = (nativeint 1) * sizeofT
        while len >= blocksize do
            copySingleBlock src dst
            src <- NativePtr.inc src
            dst <- NativePtr.inc dst
            len <- len - blocksize
        // copy remaining bytes
        let mutable dst: nativeptr<uint8> = dst |> NativePtr.cast
        let mutable src: nativeptr<uint8> = src |> NativePtr.cast
        while len >= (nativeint 0) do
            copySingleBlock src dst
            src <- NativePtr.inc src
            dst <- NativePtr.inc dst
            len <- len - (nativeint 1)

    [<Struct>]
    [<StructLayout(LayoutKind.Sequential, Pack = 1)>]
    type private uint128 =
        val lb: uint64
        val hb: uint64

    /// <summary>Copies bytes between memory locations (memcpy)</summary>
    /// <param name="pSrc">Pointer to source</param>
    /// <param name="pDst">Pointer to destination</param>
    /// <param name="length">Number of bytes to copy</param>
    [<CompiledName("Copy")>]
    [<Unverifiable>]
    let memcpy pSrc pDst length =
        memcpyBlockAligned<uint128> pSrc pDst length

    /// Copies length bytes from pSrc to pDst (memcpy)
    [<Unverifiable>]
    let internal memcpyTyped pSrc pDst (length: int64) =
       memcpy (pSrc |> NativePtr.toNativeInt) (pDst |> NativePtr.toNativeInt) (nativeint length)

    /// <summary>Copies the bytes of an array of type T to an existing array of type U</summary>
    /// <param name="src">Source array</param>
    /// <param name="dst">Destination array</param>    
    /// <typeparam name="T">Type of source array</typeparam>
    /// <typeparam name="U">Type of destination arrayr</typeparam>    
    /// <remarks>assumes that (src.Length * sizeof(T)) &#37; sizeof(U) == 0</remarks>
    [<CompiledName("Copy")>]
    [<Unverifiable>]
    let copy (src: 'T[] when 'T: unmanaged and 'T: struct) (dst: 'U[] when 'U: unmanaged and 'U: struct) =
        let sizeofT = nativeint sizeof<'T>
        let sizeofU = nativeint sizeof<'U>
        let srcLength = nativeint src.Length
        let byteCount = sizeofT * srcLength
        if byteCount % sizeofU <> (nativeint 0) then
            failwith "Violated constraint: (src.Length * sizeof(T)) % sizeof(U) == 0"        
        use pSrc = fixed &src.[0]
        use pDst = fixed &dst.[0]
        memcpy (pSrc |> NativePtr.toNativeInt) (pDst |> NativePtr.toNativeInt) byteCount
        
    /// <summary>Copies the bytes of an array of type T to a new array of type U</summary>
    /// <param name="src">Source array</param>
    /// <typeparam name="T">Type of source array</typeparam>
    /// <typeparam name="U">Type of destination arrayr</typeparam>    
    /// <returns>New array with the contents of src, interpreted as type U</returns>
    [<CompiledName("Copy")>]
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Unverifiable>]
    let inline convert (src: 'T[]): 'U[] =
        let dst = Array.zeroCreate ((src.Length * sizeof<'T>) / sizeof<'U>)
        copy src dst
        dst

    