﻿namespace NativeInteropEx

#nowarn "9" // may generate unverifiable IL

open System.Runtime.CompilerServices

/// Extension methods for System.IntPtr (nativeint)
/// (in F# the methods' this parameter is actually a typed nativeptr<'T> which however 
/// is represented as a nativeint/IntPtr outside of F#)
[<Extension>]
type IntPtrEx =    
    /// <summary>Add an offset to a pointer</summary>
    /// <param name="ptr">Base address</param>
    /// <param name="offset">Offset to be added to ptr</param>
    /// <typeparam name="T">Type of value ptr is pointing to; must be unmanged (blittable)</typeparam>
    /// <returns>(T*)ptr + offset</returns>
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]    
    static member AddOffset (ptr: nativeptr<'T>, offset: int) = ptr |> NativePtr.add offset

    /// <summary>Add an offset to a pointer</summary>
    /// <param name="ptr">Base address</param>
    /// <param name="offset">Offset to be added to ptr</param>
    /// <typeparam name="T">Type of value ptr is pointing to; must be unmanged (blittable)</typeparam>
    /// <returns>(T*)ptr + offset</returns>
    [<Extension>]
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    static member AddOffset (ptr: nativeptr<'T>, offset: int64) = ptr |> NativePtr.add offset

    /// <summary>Add an offset to a pointer</summary>
    /// <param name="ptr">Base address</param>
    /// <param name="offset">Offset to be added to ptr</param>
    /// <typeparam name="T">Type of value ptr is pointing to; must be unmanged (blittable)</typeparam>
    /// <returns>(T*)ptr + offset</returns>
    [<Extension>]
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    static member AddOffset (ptr: nativeptr<'T>, offset: nativeint) = ptr |> NativePtr.add offset
        
    /// <summary>Increment a pointer</summary>
    /// <param name="ptr">Pointer to be incremented</param>
    /// <typeparam name="T">Type of value ptr is pointing to; must be unmanged (blittable)</typeparam>
    /// <returns>(T*)ptr + 1</returns>
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Increment (ptr: nativeptr<'T>) = ptr |> NativePtr.inc
    
    /// <summary>Decrement a pointer value</summary>
    /// <param name="ptr">Pointer to be decremented</param>
    /// <typeparam name="T">Type of value ptr is pointing to; must be unmanged (blittable)</typeparam>
    /// <returns>(T*)ptr - 1</returns>
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Decrement (ptr: nativeptr<'T>) = ptr |> NativePtr.dec

    /// <summary>Read from a pointer with offset</summary>
    /// <param name="pBase">Base address</param>
    /// <param name="offset">Offset to be added to pBase</param>
    /// <typeparam name="T">Type of value to be read; must be unmanged (blittable)</typeparam>
    /// <returns>*((T*)pBase + offset)</returns>    
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Get (pBase: nativeptr<'T>, offset: int64) = pBase |> NativePtr.get offset
    
    /// <summary>Read from a pointer with offset</summary>
    /// <param name="pBase">Base address</param>
    /// <param name="offset">Offset to be added to pBase</param>
    /// <typeparam name="T">Type of value to be read; must be unmanged (blittable)</typeparam>
    /// <returns>*((T*)pBase + offset)</returns>    
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Get (pBase: nativeptr<'T>, offset: int) = pBase |> NativePtr.get offset

    /// <summary>Read from a pointer with offset</summary>
    /// <param name="pBase">Base address</param>
    /// <param name="offset">Offset to be added to pBase</param>
    /// <typeparam name="T">Type of value to be read; must be unmanged (blittable)</typeparam>
    /// <returns>*((T*)pBase + offset)</returns>    
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Get (pBase: nativeptr<'T>, offset: nativeint) = pBase |> NativePtr.get offset

    /// <summary>Write to a pointer with offset</summary>
    /// <param name="pBase">Base address</param>
    /// <param name="offset">Offset to be added to pBase</param>
    /// <param name="value">Value to be written</param>
    /// <typeparam name="T">Type of value to write; must be unmanged (blittable)</typeparam>
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Set (pBase: nativeptr<'T>, offset: int64, value) = value |> NativePtr.set pBase offset
    
    /// <summary>Write to a pointer with offset</summary>
    /// <param name="pBase">Base address</param>
    /// <param name="offset">Offset to be added to pBase</param>
    /// <param name="value">Value to be written</param>
    /// <typeparam name="T">Type of value to write; must be unmanged (blittable)</typeparam>
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Set (pBase: nativeptr<'T>, offset: int, value) = value |> NativePtr.set pBase offset

    /// <summary>Write to a pointer with offset</summary>
    /// <param name="pBase">Base address</param>
    /// <param name="offset">Offset to be added to pBase</param>
    /// <param name="value">Value to be written</param>
    /// <typeparam name="T">Type of value to write; must be unmanged (blittable)</typeparam>
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Set (pBase: nativeptr<'T>, offset: nativeint, value) = value |> NativePtr.set pBase offset
    
    /// <summary>Dereference a pointer</summary>
    /// <param name="ptr">Pointer to memory, interpreted as type T</param>
    /// <typeparam name="T">Type of value to be read; must be unmanged (blittable)</typeparam>
    /// <returns>*((T*)ptr))</returns>
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Read (ptr: nativeptr<'T>) = ptr |> NativePtr.read

    /// <summary>Write to a memory location</summary>
    /// <param name="ptr">Pointer to target location</param>
    /// <param name="value">Value to be written</param>
    /// <typeparam name="T">Type of value to be written; must be unmanged (blittable)</typeparam>
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Extension>]
    static member Write (ptr: nativeptr<'T>) value = value |> NativePtr.write ptr
