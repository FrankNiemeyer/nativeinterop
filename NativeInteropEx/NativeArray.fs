﻿namespace NativeInteropEx

#nowarn "9"

open System
open System.Runtime.InteropServices
open System.Runtime.CompilerServices
open System.Collections
open System.Collections.Generic
open System.Text

[<RequireQualifiedAccess>]
module private Util =    
    let inline boundsCheck idx len =
        if int64 idx < 0L || int64 idx >= len
            then raise <| IndexOutOfRangeException()

    let inline boundsCheckDebug idx len =
        #if DEBUG
        boundsCheck idx len
        #endif
        ()

/// Array allocated on the unmanaged heap; supports up to 2^63 elements
/// on 64 bit machines
[<Sealed>]
type NativeArray<'T when 'T: unmanaged and 'T: struct and 'T: (new: unit -> 'T) and 'T :> ValueType>(length: int64) =
    let itemSize = sizeof<'T>
    let allocatedBytes = length * int64 itemSize
    let mutable baseAddress: nativeptr<'T> = (nativeint allocatedBytes) |> Marshal.AllocHGlobal |> NativePtr.ofNativeInt
    let mutable disposed = false
    do if allocatedBytes > 0L then GC.AddMemoryPressure allocatedBytes

    new (length: int) = new NativeArray<_>(int64 length)

    /// Number of elements in the array
    member this.Length = length
    /// Address of the first element of the array
    member this.BaseAddress = baseAddress
    /// Number of bytes per element (stride)
    member this.ItemSize = itemSize
    /// Total number of bytes allocated
    member this.AllocatedBytes = allocatedBytes
    
    /// Get/set element value
    member this.Item        
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get (idx: int64) =              
            Util.boundsCheckDebug idx length
            baseAddress |> NativePtr.get idx
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set (idx: int64) (value: 'T) =
            Util.boundsCheckDebug idx length
            value |> NativePtr.set baseAddress idx
    
    /// Get/set element value
    member this.Item
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get (idx: int) =            
            Util.boundsCheckDebug idx length
            baseAddress |> NativePtr.get idx
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set (idx: int) (value: 'T) =
            Util.boundsCheckDebug idx length
            value |> NativePtr.set baseAddress idx

    /// Get/set element value with bounds check
    member this.At
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get (idx: int64) =
            Util.boundsCheck idx length
            baseAddress |> NativePtr.get idx
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set (idx: int64) (value: 'T) =
            Util.boundsCheck idx length
            value |> NativePtr.set baseAddress idx
    
    /// Get/set element value with bounds check
    member this.At
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get (idx: int) =
            Util.boundsCheck idx length
            baseAddress |> NativePtr.get idx
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set (idx: int) (value: 'T) =
            Util.boundsCheck idx length
            value |> NativePtr.set baseAddress idx

    /// Get element value with bounds check
    member this.Get (idx: int) = this.At(idx)
    /// Get element value with bounds check
    member this.Get (idx: int64) = this.At(idx)
    /// Set element value with bounds check
    member this.Set (idx: int, value: 'T) = this.At(idx) <- value
    /// Set element value with bounds check
    member this.Set (idx: int64, value: 'T) = this.At(idx) <- value
    
    member private this.Free () = 
        if not disposed then
            baseAddress |> NativePtr.toNativeInt |> Marshal.FreeHGlobal
            baseAddress <- IntPtr.Zero |> NativePtr.ofNativeInt
            if allocatedBytes > 0L then GC.RemoveMemoryPressure allocatedBytes
            disposed <- true
                                   
    override this.Finalize () = this.Free()
    
    /// Free allocated memory
    member this.Dispose () =
        this.Free()
        GC.SuppressFinalize this        
       
    interface IDisposable with
        member this.Dispose () = this.Dispose ()

    override this.ToString () =
        let sb = StringBuilder("[")
        sb.Append this.[0] |> ignore
        for x in (this :> IEnumerable<'T>) |> Seq.skip 1 do
            sb.Append ", " |> ignore
            sb.Append x    |> ignore
        sb.Append "]" |> ignore
        sb.ToString()

    interface IEnumerable<'T> with
        member this.GetEnumerator () : IEnumerator<'T> =
            new NativeArrayEnumerator<'T>(this) :> IEnumerator<'T>
        member this.GetEnumerator () : IEnumerator =
            new NativeArrayEnumerator<'T>(this) :> IEnumerator

/// Specialized enumerator for NativeArray
and [<Struct>] private NativeArrayEnumerator<'T when 'T: unmanaged and 'T: struct and 'T: (new: unit -> 'T) and 'T :> ValueType> = 
    val mutable private index: int64
    val private array: NativeArray<'T>
    new (array: NativeArray<_>) = { index = -1L; array = array }
    interface IEnumerator<'T> with
        member this.Current: 'T  = this.array.[this.index]
        member this.Current: obj = this.array.[this.index] |> box
        member this.Dispose () = ()
        
        [<MethodImpl(MethodImplOptions.AggressiveInlining)>] 
        member this.MoveNext () =
            if this.index < this.array.Length - 1L then
                this.index <- this.index + 1L
                true
            else
                false
        
        member this.Reset () = this.index <- 0L

// Forward declare to force module suffix on NativeArray module
/// Static and extension methods for creating and transforming NativeArrays
[<Sealed; AbstractClass; Extension>]
type NativeArray private () = class end

/// F# module for creating and transforming NativeArrays
[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
[<RequireQualifiedAccess>]
module NativeArray =
    /// Creates a new NativeArray<_> from a sequence expression
    let ofSeq xs =
        let cachedxs = xs |> Seq.cache
        let res = new NativeArray<'T>(cachedxs |> Seq.length)
        let mutable i = 0L
        for x in cachedxs do
            res.[i] <- x
            i <- i + 1L
        res                

    /// Creates a new NativeArray<_> by copying the elements of the supplied CLI array
    let ofArray (xs: 'T[]) =
        let len = Array.length xs
        let res = new NativeArray<'T>(len)        
        let srcHandle = GCHandle.Alloc(xs, GCHandleType.Pinned)        
        let pSrc = srcHandle.AddrOfPinnedObject() |> NativePtr.ofNativeInt<'T>
        Buffer.memcpyTyped pSrc res.BaseAddress res.AllocatedBytes
        srcHandle.Free()
        res

    /// Returns the length (number of items) of a NativeArray<_>    
    let inline length (xs: NativeArray<_>) = xs.Length

    /// Initializes a new NativeArray<_> of the given length (item count)
    /// by calling a generator function on each index [0 .. count]
    let inline init64 (count: int64) f =        
        let xs = new NativeArray<'T>(count)        
        let mutable i = 0L
        while i < count do
            xs.[i] <- f i
            i <- i + 1L
        xs              

    /// Initializes a new NativeArray<_> of the given length (item count)
    /// by calling a generator function on each index [0 .. count]
    let inline init (count: int) f =        
        let xs = new NativeArray<'T>(count)
        for i = 0 to count - 1 do
            xs.[i] <- f i
        xs

    /// Creates a NativeArray<'T> with all elements set to Unchecked.defaultof<'T>
    let zeroCreate64<'T when 'T: unmanaged and 'T: struct and 'T :> ValueType and 'T: (new: unit -> 'T)> (count: int64) =        
        let xs = new NativeArray<'T>(count)
        let zero = Unchecked.defaultof<'T>
        let mutable i = 0L
        while i < count do
            xs.[i] <- zero
            i <- i + 1L
        xs

    /// Creates a NativeArray<_> with all elements set to Unchecked.defaultof<_>
    let zeroCreate<'T when 'T: unmanaged and 'T: struct and 'T :> ValueType and 'T: (new: unit -> 'T)> (count: int) =        
        let xs = new NativeArray<'T>(count)
        for i = 0 to count - 1 do
            xs.[i] <- Unchecked.defaultof<'T>
        xs

    /// Copies the contents of on NativeArray<_> to a new instance
    let copy (xs: NativeArray<'T>) =
        let res = new NativeArray<'T>(xs.Length)
        Buffer.memcpyTyped xs.BaseAddress res.BaseAddress res.AllocatedBytes
        res
    
    /// Copies the contents of on NativeArray<'a> to a new NativeArray<'b>
    let convert (xs: NativeArray<'T>) =
        let res = new NativeArray<'U>(xs.AllocatedBytes / int64 sizeof<'U>)
        if xs.AllocatedBytes <> res.AllocatedBytes then
            failwith "Violated constraint: xs.AllocatedBytes % sizeof<'T> <> 0"
        Buffer.memcpyTyped xs.BaseAddress res.BaseAddress xs.AllocatedBytes
        res
    
    /// Get a view on an existing array of 'T items as an array of items of type 'U
    //let reinterpretCast (xs: NativeArray<'T>): NativeArray<'U> =

    /// Resizes an array by copying the contents to a new array of length newSize
    let resize64 (newLength: int64) (xs: NativeArray<'T>) =
        if (newLength < xs.Length) then
            failwith "newLength must be >= old length"
        let res = new NativeArray<'T>(newLength)
        Buffer.memcpyTyped xs.BaseAddress res.BaseAddress xs.AllocatedBytes
        res

    let resize (newLength: int) (xs: NativeArray<'T>) =
        xs |> resize64 (int64 newLength)

    /// Maps a NativeArray<_> to a new NativeArray<_> by applying
    /// a given transformation to all elements
    let inline map (f: 'T -> 'U) xs =     
        let len = length xs   
        let res = new NativeArray<_>(len)
        let mutable i = 0L
        while i < len do
            res.[i] <- f xs.[i]
            i <- i + 1L
        res        

    /// Maps a NativeArray<_> to a new NativeArray<_> by applying
    /// a given transformation that depends on the index of an element to all elements
    let inline mapi (f: int64 -> 'T -> 'U) xs =     
        let len = length xs   
        let res = new NativeArray<_>(len)
        let mutable i = 0L
        while i < len do
            res.[i] <- f i xs.[i]
            i <- i + 1L
        res

    /// Applies a given transformation to all elements of a NativeArray<_>
    let inline mapInplace (f: 'T -> _) xs =                
        let len = length xs   
        let mutable i = 0L
        while i < len do
            xs.[i] <- f xs.[i]
            i <- i + 1L
        xs

    /// Applies a given transformation, which depends on the element index,
    /// to all elements of a NativeArray<_>
    let inline mapiInplace (f: _ -> 'T -> _) xs =        
        let len = length xs   
        let mutable i = 0L
        while i < len do
            xs.[i] <- f i xs.[i]
            i <- i + 1L
        xs

    /// Maps the elements of two input arrays onto a new array
    let inline map2 (f: 'T -> 'U -> 'V) (xs: NativeArray<'T>) (ys: NativeArray<'U>) =     
        let len = length xs   
        let res = new NativeArray<_>(len)
        let mutable i = 0L
        while i < len do
            res.[i] <- f xs.[i] ys.[i]
            i <- i + 1L
        res  

    /// Applies a function to each element of the array, threading a accumulator
    /// element through the computation
    let inline fold (f: 'State -> 'T -> 'State) state xs = 
        let mutable res = state
        let len = length xs
        let mutable i = 0L
        while i < len do
            res <- f res xs.[i]
            i <- i + 1L
        res                
    
    /// Combines all elements of the input array into an accumulated value
    let inline reduce f (xs: NativeArray<'T>) =
        if xs.Length < 1L then
            failwith "Cannot reduce array of size 0"
        let mutable res = xs.[0]
        let len = length xs
        let mutable i = 1L
        while i < len do
            res <- f res xs.[i]
            i <- i + 1L
        res      
    
    /// Applies a function to each pair of elements of two arrays, threading a accumulator
    /// element through the computation
    let inline fold2 (f: 'State -> 'T -> 'U -> 'State) state xs (ys: NativeArray<_>) = 
        let mutable res = state
        let len = length xs
        let mutable i = 0L
        while i < len do
            res <- f res xs.[i] ys.[i]
            i <- i + 1L
        res

    /// Sum of the elements of a numeric array
    let inline sum (xs: NativeArray<'T>) = xs |> reduce (+)
    
    /// Product of the elements of a numeric array
    let inline prod (xs: NativeArray<'T>) = xs |> reduce (*)
    
    /// Dot-product of two numeric arrays
    let inline dot (xs: NativeArray<'T>) (ys: NativeArray<'T>): 'a =
        fold2 (fun dp x y -> dp + x * y) LanguagePrimitives.GenericZero xs ys        

    /// Returns a new NativeArray<_> for which all
    /// elements satisfy the provided predicate
    let inline filter (p: 'T -> bool) (xs: NativeArray<'T>) = //xs |> Seq.filter p |> ofSeq
        use predicates = xs |> map (fun x -> match p x with
                                             | true  -> 1uy
                                             | false -> 0uy)
        let filteredLen = predicates |> fold (fun acc x -> acc + int64 x) 0L
        let res = new NativeArray<'T>(filteredLen)
        let fullLen = predicates |> length
        let mutable i = 0L
        let mutable j = 0L
        while i < fullLen do
            match predicates.[i] with
            | 1uy -> res.[j] <- xs.[i]
                     j <- j + 1L
            | 0uy -> ()
            | _   -> failwith "Expected 0/1 truth value"
            i <- i + 1L
        res
    
    //let append (xs: NativeArray<_>) (ys: NativeArray<_>) =
    //sort, find, ...

type NativeArray with 
    /// <summary>
    /// Initializes a new array mapping each index i to a value f(i)
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    /// <param name="f">Function mapping indices to initial values</param>
    static member Initialize (length: int, f: Func<int, 'T>) =
        NativeArray.init length f.Invoke

    /// <summary>
    /// Initializes a new array mapping each index i to a value f(i)
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    /// <param name="f">Function mapping indices to initial values</param>
    static member Initialize (length: int64, f: Func<int64, 'T>) =
        NativeArray.init64 length f.Invoke

    /// <summary>
    /// Creates a new array with all elements initialized to their default value
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    static member ZeroCreate (length: int): NativeArray<'T> =
        NativeArray.zeroCreate length

    /// <summary>
    /// Creates a new array with all elements initialized to their default value
    /// </summary>
    /// <param name="length">Number of items in the new array</param>
    static member ZeroCreate (length: int64): NativeArray<'T> =
        NativeArray.zeroCreate64 length

    /// <summary>
    /// Creates a new NativeArray by copying the elements of a managed array
    /// </summary>
    /// <param name="array">Managed array to copy</param>
    static member From (array: 'T[]): NativeArray<'T> =
        NativeArray.ofArray array        

    /// <summary>
    /// Creates a new array from an enumerable sequence
    /// </summary>
    /// <param name="seq">Sequence from which to generate the array's elements</param>
    static member From (seq: seq<'T>): NativeArray<'T> =
        NativeArray.ofSeq seq

    /// <summary>
    /// Sums the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Sum (this: NativeArray<float>) =
        this |> NativeArray.sum

    /// <summary>
    /// Sums the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Sum (this: NativeArray<float32>) =
        this |> NativeArray.sum

    /// <summary>
    /// Sums the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Sum (this: NativeArray<int>) =
        this |> NativeArray.sum

    /// <summary>
    /// Sums the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Sum (this: NativeArray<int64>) =
        this |> NativeArray.sum

    /// <summary>
    /// Sums the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Sum (this: NativeArray<uint32>) =
        this |> NativeArray.sum

    /// <summary>
    /// Sums the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Sum (this: NativeArray<uint64>) =
        this |> NativeArray.sum

    /// <summary>
    /// Sums the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Sum (this: NativeArray<decimal>) =
        this |> NativeArray.sum
    
    /// <summary>
    /// Computes the product of the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Product (this: NativeArray<float>) =
        this |> NativeArray.prod

    /// <summary>
    /// Computes the product of the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Product (this: NativeArray<float32>) =
        this |> NativeArray.prod

    /// <summary>
    /// Computes the product of the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Product (this: NativeArray<int>) =
        this |> NativeArray.prod

    /// <summary>
    /// Computes the product of the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Product (this: NativeArray<int64>) =
        this |> NativeArray.prod
        
    /// <summary>
    /// Computes the product of the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Product (this: NativeArray<uint32>) =
        this |> NativeArray.prod
                
    /// <summary>
    /// Computes the product of the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Product (this: NativeArray<uint64>) =
        this |> NativeArray.prod
                
    /// <summary>
    /// Computes the product of the elements of a numeric array
    /// </summary>
    [<Extension>]
    static member Product (this: NativeArray<decimal>) =
        this |> NativeArray.prod
                
    /// <summary>
    /// Computes the dot product of two numeric arrays
    /// </summary>
    [<Extension>]
    static member Dot (this: NativeArray<float>, other: NativeArray<float>) =
        NativeArray.dot this other
                
    /// <summary>
    /// Computes the dot product of two numeric arrays
    /// </summary>
    [<Extension>]
    static member Dot (this: NativeArray<float32>, other: NativeArray<float32>) =
        NativeArray.dot this other
                
    /// <summary>
    /// Computes the dot product of two numeric arrays
    /// </summary>
    [<Extension>]
    static member Dot (this: NativeArray<int>, other: NativeArray<int>) =
        NativeArray.dot this other
                
    /// <summary>
    /// Computes the dot product of two numeric arrays
    /// </summary>
    [<Extension>]
    static member Dot (this: NativeArray<int64>, other: NativeArray<int64>) =
        NativeArray.dot this other
                
    /// <summary>
    /// Computes the dot product of two numeric arrays
    /// </summary>
    [<Extension>]
    static member Dot (this: NativeArray<uint32>, other: NativeArray<uint32>) =
        NativeArray.dot this other
                
    /// <summary>
    /// Computes the dot product of two numeric arrays
    /// </summary>
    [<Extension>]
    static member Dot (this: NativeArray<uint64>, other: NativeArray<uint64>) =
        NativeArray.dot this other
                
    /// <summary>
    /// Computes the dot product of two numeric arrays
    /// </summary>
    [<Extension>]
    static member Dot (this: NativeArray<decimal>, other: NativeArray<decimal>) =
        NativeArray.dot this other

    /// <summary>
    /// Copies a NativeArray instance
    /// </summary>
    [<Extension>]
    static member Copy (this: NativeArray<'T>) =
        this |> NativeArray.copy

    /// <summary>
    /// Resizes a NativeArray instance
    /// </summary>
    [<Extension>]
    static member Resize (this: NativeArray<'T>, newLength: int64) =
        this |> NativeArray.resize64 newLength

    /// <summary>
    /// Resizes a NativeArray instance
    /// </summary>
    [<Extension>]
    static member Resize (this: NativeArray<'T>, newLength: int) =
        this |> NativeArray.resize newLength

    /// <summary>
    /// Copies the contents of a NativeArray to a NativeArray of a different type
    /// </summary>
    [<Extension>]
    static member Convert (this: NativeArray<'T>): NativeArray<'U> =
        this |> NativeArray.convert

    /// <summary>
    /// Transforms the elements of an array using a given mapping
    /// </summary>
    /// <param name="f">Element mapping</param>    
    [<Extension>]
    static member Map (this: NativeArray<'T>, f: Func<'T, 'U>): NativeArray<'U> =
        this |> NativeArray.map f.Invoke
    
    /// <summary>
    /// Transforms the elements of an array using a given mapping
    /// </summary>
    /// <param name="f">Element mapping</param>    
    [<Extension>]
    static member Map (this: NativeArray<'T>, f: Func<int64, 'T, 'T>) =
        this |> NativeArray.mapi (fun i x -> f.Invoke(i, x))

    /// <summary>
    /// Transforms the elements of an array in-place, modifying the current instance
    /// </summary>
    /// <param name="f">Element mapping</param>    
    [<Extension>]
    static member MapInplace (this: NativeArray<'T>, f: Func<'T, 'T>) =
        this |> NativeArray.mapInplace f.Invoke

    /// <summary>
    /// Transforms the elements of an array in-place, modifying the current instance
    /// </summary>
    /// <param name="f">Element mapping</param>    
    [<Extension>]
    static member MapInplace (this: NativeArray<'T>, f: Func<int64, 'T, 'T>) =
        this |> NativeArray.mapiInplace (fun i x -> f.Invoke(i, x))
    
    /// <summary>
    /// Transforms the paired elements of two arrays
    /// </summary>
    /// <param name="other">Second array to map over</param>    
    /// <param name="f">Element mapping</param>    
    [<Extension>]
    static member Map2 (this: NativeArray<'T>, other: NativeArray<'U>, f: Func<'T, 'U, 'V>) =
        NativeArray.map2 (fun t v -> f.Invoke(t, v)) this other

    /// <summary>
    /// Reduces the elements of a NativeArray to an accumulated value
    /// </summary>
    /// <param name="f">Element accumulation function</param>    
    [<Extension>]
    static member Reduce (this: NativeArray<'T>, f: Func<'T, 'T, 'T>) =
        this |> NativeArray.reduce (fun a b -> f.Invoke(a, b))

    /// <summary>
    /// Applies a function to each element of the array, threading a accumulator
    /// element through the computation
    /// </summary>
    /// <param name="f">Folding function</param>
    /// <param name="state">Initial value of the accumulator</param>
    [<Extension>]
    static member Fold (this: NativeArray<'T>, f: Func<'U, 'T, 'U>, state: 'U) =
        this |> NativeArray.fold (fun s x -> f.Invoke(s, x)) state

    /// <summary>
    /// Applies a function to each pair of elements of two arrays, threading a accumulator
    /// element through the computation
    /// </summary>
    /// <param name="f">Folding function</param>
    /// <param name="state">Initial value of the accumulator</param>
    /// <param name="other">Second array</param>
    [<Extension>]
    static member Fold2 (this: NativeArray<'T>, other: NativeArray<'U>, f: Func<'State, 'T, 'U, 'State>, state: 'State) =
        NativeArray.fold2 (fun s x y -> f.Invoke(s, x, y)) state this other

    /// <summary>
    /// Applies a function to each pair of elements of two arrays, threading a accumulator
    /// element through the computation
    /// </summary>
    /// <param name="f">Folding function</param>
    /// <param name="state">Initial value of the accumulator</param>
    /// <param name="other">Second array</param>
    [<Extension>]
    static member ToNativeArray (this: IEnumerable<'T>) =
        this |> NativeArray.ofSeq
