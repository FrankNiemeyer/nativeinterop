﻿namespace NativeInteropEx

#nowarn "9" // may generate unverifiable IL

open System
open System.Runtime.InteropServices
open System.Runtime.CompilerServices

/// Extended version of Microsoft.FSharp.NativeInterop from FSharp.Core.dll.
/// Usable from F# codebases only (requires inline methods), non-F#
/// users should use IntPtrEx and/or NativePtr<'T> instead
[<RequireQualifiedAccess>]
module NativePtr =
    open Microsoft.FSharp.NativeInterop
    
    /// <summary>Reinterpret a pointer of type nativeptr&lt;'T&gt; as nativeptr&lt;'U&gt;</summary>
    /// <param name="ptr">Pointer to be casted</param>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline cast<'T, 'U when 'U : unmanaged and 'T: unmanaged> (ptr: nativeptr<'T>) =
        ptr |> NativePtr.toNativeInt |> NativePtr.ofNativeInt<'U>
    
    /// <summary>Add an offset to a pointer</summary>
    /// <param name="ptr">Base address</param>
    /// <param name="offset">Offset to be added to ptr</param>
    /// <returns>(T*)ptr + offset</returns>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline add offset (ptr: nativeptr<'T>): nativeptr<'T> = 
        NativePtr.toNativeInt ptr + nativeint offset * nativeint sizeof<'T> |> NativePtr.ofNativeInt

    /// <summary>Increment a pointer</summary>
    /// <param name="ptr">Pointer to be incremented</param>
    /// <returns>(T*)ptr + 1</returns>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline inc (ptr: nativeptr<'T>) : nativeptr<'T> = NativePtr.toNativeInt ptr + nativeint sizeof<'T> |> NativePtr.ofNativeInt
    
    /// <summary>Decrement a pointer value</summary>
    /// <param name="ptr">Pointer to be decremented</param>
    /// <returns>(T*)ptr - 1</returns>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline dec (ptr: nativeptr<'T>): nativeptr<'T> = NativePtr.toNativeInt ptr - nativeint sizeof<'T> |> NativePtr.ofNativeInt
    
    /// <summary>Dereference a pointer</summary>
    /// <param name="ptr">Pointer to memory, interpreted as type T</param>
    /// <typeparam name="T">Type of value to read; must be unmanged (blittable)</typeparam>
    /// <returns>*((T*)ptr))</returns>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline read (ptr: nativeptr<'T>) = NativePtr.read ptr
    
    /// <summary>Write to a memory location</summary>
    /// <param name="ptr">Pointer to target location</param>
    /// <param name="value">Value to be written</param>
    /// <typeparam name="T">Type of value to be written; must be unmanged (blittable)</typeparam>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline write (ptr: nativeptr<'T>) value = NativePtr.write ptr value

    /// <summary>Read from a pointer with offset</summary>
    /// <param name="ptr">Base address</param>
    /// <param name="offset">Offset to be added to ptr</param>
    /// <typeparam name="T">Type of value to read; must be unmanged (blittable)</typeparam>
    /// <returns>*((T*)ptr + offset)</returns>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline get offset (pBase: nativeptr<'T>): 'T = pBase |> add offset |> read

    /// <summary>Write to a pointer with offset</summary>
    /// <param name="ptr">Base address</param>
    /// <param name="offset">Offset to be added to ptr</param>
    /// <param name="value">Value to be written</param>
    /// <typeparam name="T">Type of value to write; must be unmanged (blittable)</typeparam>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline set (pBase: nativeptr<'T>) offset (value: 'T) = value |> write (pBase |> add offset)

    /// <summary>Convert nativeint to a typed nativeptr&lt;'T&gt;</summary>
    /// <param name="n">value to be converted</param>
    /// <typeparam name="T">Type of value the resulting pointer is considered 
    /// pointing to; must be unmanged (blittable)</typeparam>
    /// <remarks>This is a no-op in C#, as F#'s nativeptr&lt;'T&gt; is represented as IntPtr (nativeint) in CIL</remarks>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline ofNativeInt<'T when 'T: unmanaged> n: nativeptr<'T> = NativePtr.ofNativeInt n
    
    /// <summary>Convert a typed nativeptr&lt;'T&gt; to a nativeint</summary>
    /// <param name="p">value to be converted</param>
    /// <typeparam name="T">Type of value the input pointer is considered 
    /// pointing to; must be unmanged (blittable)</typeparam>
    /// <remarks>This is a no-op in C#, as F#'s nativeptr&lt;'T&gt; is represented as IntPtr (nativeint) in CIL</remarks>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline toNativeInt (p: nativeptr<'T>) = NativePtr.toNativeInt p

    /// <summary>Allocates memory on the stack</summary>
    /// <param name="n">number of items to allocate</param>
    /// <typeparam name="T">Type of the allocated values; must be unmanged (blittable)</typeparam>
    /// <remarks>*Must* be inlined to work, must not be called from outside F#!</remarks>
    /// <returns>Pointer to location on stack</returns>
    [<Unverifiable>]
    [<NoDynamicInvocation>]
    let inline stackalloc n: nativeptr<'T> = NativePtr.stackalloc n

/// OO interface for C# and VB
[<Struct>]
type NativePtr<'T when 'T: unmanaged>(pointer : nativeptr<'T>) =
    new (pointer: ilsigptr<'T>) = NativePtr(pointer)
    
    member self.RawPointer = pointer
    
    member self.Value
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get () = 
            NativePtr.read pointer
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set  x = 
            NativePtr.write pointer x 
    
    member self.Item
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get idx =
            pointer |> NativePtr.get idx            
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set idx value =
            value |> NativePtr.set pointer idx
    
    member self.Item
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get (idx: int64) =
            pointer |> NativePtr.get idx 
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set (idx: int64) value =
            value |> NativePtr.set pointer idx

    member self.Item
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get (idx: nativeint) =
            pointer |> NativePtr.get idx 
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set (idx: nativeint) value =
            value |> NativePtr.set pointer idx

    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    static member inline (+) (p: NativePtr<'T>, offset) = p.RawPointer |> NativePtr.add offset |> NativePtr

    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    static member inline (+) (p: NativePtr<'T>, offset: int64) = p.RawPointer |> NativePtr.add offset |> NativePtr

    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    static member inline (+) (p: NativePtr<'T>, offset: nativeint) = p.RawPointer |> NativePtr.add offset |> NativePtr

    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    static member inline (-) (p: NativePtr<'T>, offset) = p.RawPointer |> NativePtr.add -offset |> NativePtr

    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    static member inline (-) (p: NativePtr<'T>, offset: int64) = p.RawPointer |> NativePtr.add -offset |> NativePtr

    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    static member inline (-) (p: NativePtr<'T>, offset: nativeint) = p.RawPointer |> NativePtr.add -offset |> NativePtr

