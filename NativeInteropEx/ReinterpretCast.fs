﻿namespace NativeInteropEx

#nowarn "9" // may generate unverifiable IL

open System
open System.Runtime.InteropServices
open System.Runtime.CompilerServices

/// Access object of type T through a U pointer
type ReinterpretCast<'T, 'U when 'U: unmanaged>(x: 'T) =
    let handle = GCHandle.Alloc(x, GCHandleType.Pinned)
    let mutable ptr = handle.AddrOfPinnedObject() |> NativePtr.ofNativeInt |> NativePtr<'U>
    
    /// Get pointer to pinned object
    member self.Pointer = ptr

    member self.Value
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get () = ptr.Value
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set x  = ptr.Value <- x
    
    member self.Item
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get (idx: int) =
            ptr.[idx]            
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set (idx: int) value =
            ptr.[idx] <- value
    
    member self.Item
        with [<MethodImpl(MethodImplOptions.AggressiveInlining)>] get (idx: int64) =
            ptr.[idx]
        and  [<MethodImpl(MethodImplOptions.AggressiveInlining)>] set (idx: int64) value =
            ptr.[idx] <- value
    
    member private this.Free () = if handle.IsAllocated then handle.Free()
    override this.Finalize () = this.Free()
    member this.Dispose () =
        this.Free()
        GC.SuppressFinalize this  

    interface IDisposable with
        member this.Dispose () = this.Dispose ()
