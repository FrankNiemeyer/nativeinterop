﻿namespace NativeInteropEx

#nowarn "9" // may generate unverifiable IL

open System.IO    
open System.Runtime.CompilerServices

[<RequireQualifiedAccess>]
[<Extension>]
module Stream =
    
    /// <summary>Write a single unmanaged struct to a Stream</summary>
    /// <param name="stream">Stream to be written to</param>    
    /// <param name="structure">Struct value to write to stream</param>    
    /// <typeparam name="T">Type of structure</typeparam>
    /// <remarks>Allocates a temporary write buffer</remarks>    
    [<Extension>]
    [<CompiledName("WriteUnmanagedStruct")>]
    [<Unverifiable>]
    let writeStructure (stream: Stream) (structure: 'T) =    
        let structureBytes = structure |> Structure.toBuffer         
        stream.Write(structureBytes, 0, Array.length structureBytes)

    /// <summary>Write an array of unmanaged structs to a Stream</summary>
    /// <param name="stream">Stream to be written to</param>    
    /// <param name="structure">Array of struct values to write to stream</param>    
    /// <typeparam name="T">Type of structures</typeparam>
    /// <remarks>Allocates a temporary write byte buffer of the same size as structs</remarks>
    [<Extension>]
    [<CompiledName("WriteUnmanagedStructRange")>]
    [<Unverifiable>]
    let writeStructureArray (stream: Stream) (structs: 'T[]) =
        let buffer: byte[] = Buffer.convert structs
        stream.Write(buffer, 0, buffer.Length)

    /// Read a single struct value of unmanaged type T from a Stream         
    [<Extension>]
    [<CompiledName("ReadUnmanagedStruct")>]
    [<Unverifiable>]
    let readStructure (stream: Stream): 'T =
        let buffSize = sizeof<'T>
        let structBytes: byte[] = Array.zeroCreate buffSize
        // Stream.Read may return 0..buffSize -> loop, if neccessary
        let mutable readBytes = 0
        while readBytes < buffSize do
            readBytes <- readBytes + stream.Read(structBytes, readBytes, buffSize - readBytes)            
        structBytes |> Structure.fromBuffer
    
    /// <summary>Read an array of structs of unmanaged type T from a Stream</summary>
    /// <remarks>ReadUnmanagedStructRange is more efficient then calling ReadUnmanagedStruct 
    /// for each individual struct
    /// </remarks>
    /// <typeparam name="T">Specifies the type as which the read data is interpreted as</typeparam>
    /// <param name="stream"><cref see="System.IO.Stream"/> object to read from</param>
    /// <param name="count">Number of structs to be read</param>
    [<Extension>]
    [<CompiledName("ReadUnmanagedStructRange")>]
    [<Unverifiable>]
    let readStructureArray (stream: Stream) count: 'T[] =
        let structures: 'T[] = Array.zeroCreate count
        let structSize = sizeof<'T>
        let readBuffer: byte[] = Array.zeroCreate structSize
        use readBufferPtr = fixed &readBuffer.[0]        
        // read structs of type 'T from stream
        for i = 0 to count - 1 do
            // read next struct from stream into byte[] buffer
            let mutable readBytes = 0
            while readBytes < structSize do
                readBytes <- readBytes + stream.Read(readBuffer, readBytes, structSize - readBytes)        
            // store buffer as a value of type 'T in result array
            structures.[i] <- readBufferPtr |> NativePtr.cast |> NativePtr.read
        structures
