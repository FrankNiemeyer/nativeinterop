﻿namespace NativeInteropEx

#nowarn "9" // may generate unverifiable IL

open System.Runtime.CompilerServices
open System.Runtime.InteropServices

module Structure =
    /// <summary>Creates a struct of unmanaged type U from the bytes of a struct of unmanaged type T</summary>
    /// <param name="src">Reference to source struct</param>
    /// <typeparam name="T">Type of the source struct</typeparam>
    /// <typeparam name="U">Type of the result struct</typeparam>
    /// <returns>src copied to a struct of type U, i.e. var res = *((U*)&amp;src)</returns>
    /// <remarks>performs *((U*)src); requires sizeof(T) == sizeof(U)</remarks>
    [<CompiledName("Read")>]
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Unverifiable>]
    let inline read (src: byref<'T> when 'T: unmanaged and 'T: struct): 'U when 'U: unmanaged and 'U: struct =
        if sizeof<'T> <> sizeof<'U> then
            failwith "sizeof<T> != sizeof<U>"
        &&src |> NativePtr.cast |> NativePtr.read

    /// <summary>Writes a value of unmanaged type U to a struct of unmanaged type T</summary>
    /// <param name="target">Reference to target memory location</param>
    /// <param name="value">Value to be written to target</param>
    /// <typeparam name="T">Type of the target struct</typeparam>
    /// <typeparam name="U">Type of the source struct</typeparam>
    /// <remarks>performs *((U*)target) = value; requires sizeof(T) == sizeof(U)</remarks>
    [<CompiledName("Write")>]
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Unverifiable>]
    let inline write (target: byref<'T> when 'T: unmanaged and 'T: struct) (value: 'U when 'U: unmanaged) =
        if sizeof<'T> <> sizeof<'U> then
            failwith "sizeof<T> != sizeof<U>"
        NativePtr.write (&&target |> NativePtr.cast) value

    /// <summary>Read from a structure of type T by interpreting it as a pointer to values of type U</summary>
    /// <param name="structure">Reference to struct to be read from</param>
    /// <param name="offset">Offset from the start of structure in units of sizeof(U)</param>
    /// <typeparam name="T">Type of structure</typeparam>
    /// <typeparam name="U">Type of value to be read from structure</typeparam>
    /// <remarks>performs *((U*)structure + offset); requires (offset + 1) * sizeof(U) &lt;= sizeof(T)</remarks>
    [<CompiledName("Get")>]
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Unverifiable>]
    let get (structure: byref<'T> when 'T: unmanaged and 'T: struct) offset: 'U when 'U: unmanaged and 'U: struct =
        if (offset + 1) * sizeof<'U> > sizeof<'T> then
            failwith "Cannot read beyond the end of a struct"
        &&structure |> NativePtr.cast |> NativePtr.get offset
    
    /// <summary>Write to a structure of type T by interpreting it as a pointer to values of type U</summary>
    /// <param name="structure">Reference to struct to be written to</param>
    /// <param name="offset">Offset from the start of structure in units of sizeof(U)</param>
    /// <typeparam name="T">Type of structure</typeparam>
    /// <typeparam name="U">Type of value to be written to structure</typeparam>
    /// <remarks>performs *((U*)structure + offset) = value; requires (offset + 1) * sizeof(U) &lt;= sizeof(T)</remarks>    
    [<CompiledName("Set")>]
    [<MethodImpl(MethodImplOptions.AggressiveInlining)>]
    [<Unverifiable>]
    let set (structure: byref<'T> when 'T: unmanaged and 'T: struct) offset (value: 'U when 'U: unmanaged and 'U: struct) =
        if (offset + 1) * sizeof<'U> > sizeof<'T> then
            failwith "Cannot read beyond the end of a struct"
        value |> NativePtr.set (&&structure |> NativePtr.cast) offset    
    
    /// <summary>Copy an object of unmanaged type T to an existing array of type U</summary>
    /// <param name="structure">Struct value to be copied</param>
    /// <param name="buffer">Array structure is to be copied to</param>
    /// <typeparam name="T">Type of structure</typeparam>
    /// <typeparam name="U">Type of buffer</typeparam>
    /// <remarks>performs *((T*)buffer) = structure; requires buffer.Length * sizeof(U) == sizeof(T)</remarks>    
    [<CompiledName("ToBuffer")>]
    [<Unverifiable>]
    let copyToBuffer (structure: 'T when 'T: unmanaged and 'T: struct) (buffer: 'U[] when 'U: unmanaged and 'U: struct) =
        if buffer.Length * sizeof<'U> <> sizeof<'T> then
            failwith "Violated constraint: buffer.Length * sizeof(U) == sizeof(T)"
        use pBuff = fixed &buffer.[0]
        structure |> NativePtr.write (pBuff |> NativePtr.cast)        

    /// <summary>Create a new array of type U containing the bytes of an object of unmanaged type T</summary>
    /// <param name="structure">Struct value to be copied</param>
    /// <typeparam name="T">Type of structure</typeparam>
    /// <typeparam name="U">Type of buffer</typeparam>
    /// <remarks>performs *((T*)buffer) = structure; requires sizeof(U) &lt;= sizeof(T) and sizeof(T) &#37; sizeof(U) = 0</remarks>    
    /// <returns>The contents of structure as an array of type U</returns>
    [<CompiledName("ToBuffer")>]
    [<Unverifiable>]
    let toBuffer (structure: 'T) =
        let sizeofT = sizeof<'T>
        let sizeofU = sizeof<'U>
        if sizeofT < sizeofU then
            failwith "Violated constraint: sizeof(T) >= sizeof(U)"
        if sizeofT % sizeofU <> 0 then
            failwith "Violated constraint: sizeof(T) % sizeof(U) = 0"            
        let buffer: 'U[] = Array.zeroCreate (sizeof<'T> / sizeof<'U>)
        copyToBuffer structure buffer 
        buffer

    /// <summary>Read the contents of a T[] buffer as a single unmanaged value of type U</summary>
    /// <param name="structure">Reference to struct to be copied</param>
    /// <typeparam name="T">Type of buffer</typeparam>
    /// <typeparam name="U">Type of value to be read</typeparam>
    /// <remarks>performs *((U*)buffer); requires buffer.Length * sizeof(T) == sizeof(U)</remarks>
    /// <returns>The contents of buffer as a struct value of type U</returns>
    [<CompiledName("FromBuffer")>]
    [<Unverifiable>]
    let fromBuffer (buffer: 'T[] when 'T: unmanaged and 'T: struct): 'U when 'U: unmanaged and 'U: struct =
        if buffer.Length * sizeof<'T> <> sizeof<'U> then
            failwith "Violated constraint: buffer.Length * sizeof(T) == sizeof(U)"
        use pBuff = fixed &buffer.[0]
        pBuff |> NativePtr.cast |> NativePtr.read

    /// <summary>Read a U value from an array of type T[]</summary>
    /// <param name="buffer">Array to be read from</param>
    /// <param name="offset">Offset to be added to buffer's base address in units of sizeof(T)</param>
    /// <typeparam name="T">Type of buffer</typeparam>
    /// <typeparam name="U">Type of value to be read</typeparam>
    /// <remarks>performs *((U*)(buffer + offset)); requires (buffer.Length - offset) * sizeof(T) >= sizeof(U)</remarks>
    /// <returns>The contents of (buffer + offset) as a struct value of type U</returns>
    [<CompiledName("FromBuffer")>]
    [<Unverifiable>]
    let fromBufferWithOffset (buffer: 'T[] when 'T: unmanaged and 'T: struct) (offset: int): 'U when 'U: unmanaged and 'U: struct =
        if (buffer.Length - offset) * sizeof<'T> < sizeof<'U> then
            failwith "Violated constraint: (buffer.Length - offset) * sizeof(T) >= sizeof(U)"
        use pBuff = fixed &buffer.[0]
        pBuff |> NativePtr.add offset |> NativePtr.cast |> NativePtr.read
        